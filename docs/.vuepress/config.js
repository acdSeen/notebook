module.exports={
  title:'Linhong Note',
  description:'Take notes',
  head: [
    ['link', { rel: 'icon', href: '/favicon.ico' }]
  ],
  themeConfig: {
    // repo: '', // git仓库地址
    // editLinks: true, // 是否可编辑
    // editLinkText:'在 GitHub 上编辑此页',
    // lastUpdated: '上次更新',
    // displayAllHeaders: true, // 显示所有页面的标题链接
    sidebar: 'auto', // 自动生成一个仅仅包含了当前页面标题侧栏
    smoothScroll: true,
    nav: [
      { text: 'JS基础', 
        link:'/es/',
        // items: [
        //   { text:'数据类型',
        //     items:[
        //       { text: '数组', link: '/es/types/array' },
        //       { text: '字符串', link: '/es/types/string' },
        //     ]
        //   },
        //   {text:'正则相关',link:'/es/regex/regex'}       
        // ]
      },
      { text: '计算机基础', link: '/computer/concept' },
    ],
    sidebar: {
      '/es/':[
        {
          title:'概述',
          collapsable: false,
          children:[
            ['','Instruction']
          ],
        },
        {
          title: '数据类型',
          collapsable: false,
          children: [
            'types/array',
            'types/function',
            'types/general',
            'types/null-undefined-boolean',
            'types/number',
            'types/object',
            'types/string',
          ]
        },
        {
          title: '异步',
          collapsable: false,
          children: [
            'async/history',
            'async/promise'
          ]
        },
        {
          title: '正则',
          collapsable: false,
          children: [
            'regex/regex',
            'regex/quickchecktable'
          ]
        },
        {
          title: '标准库',
          collapsable: false,
          children: [
            'stdlib/array',
            'stdlib/attributes',
            'stdlib/date',
            'stdlib/math',
            'stdlib/object',
            'stdlib/string',
            'stdlib/wrapper'
          ]
        },
        {
          title: '面向对象',
          collapsable: false,
          children: [
            'oop/oop',
            'oop/new',
            'oop/object',
            'oop/prototype',
          ]
        }
      ],
    }
  },
}