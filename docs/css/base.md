# 盒模型

## 基本概念：标准模型+IE 模型

包括 content、padding、border、
margin 几个要素。
IE 模型和标准模型唯一的区别是计算方式的不同。
css3 新增属性`box-sizing:content-box | border-box`可设置盒模型为`标准模型`和`IE模型`
## 边距重叠问题