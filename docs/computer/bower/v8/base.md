# 垃圾回收机制

```javascript
node --v8-options
// 设置新生代内存中单个半空间的内存最小值，单位MB
node --min-semi-space-size=1024 xxx.js

// 设置新生代内存中单个半空间的内存最大值，单位MB
node --max-semi-space-size=1024 xxx.js

// 设置老生代内存最大值，单位MB
node --max-old-space-size=2048 xxx.js
```
使用`process.memoryUsage()`可以查看当前node进程所占用的实际内存大小。

```js
{
  rss: 20238336, // （resident set size）表示驻留集大小，是给这个node进程分配了多少物理内存，这些物理内存包含堆，栈和代码片段。对象，闭包存于堆内存，变量存于栈内存，js源代码存于代码内存。使用worker线程时，rss是一个对整个进程有效的值，而其他字段则只针对当前线程。
  heapTotal: 4907008, // 表示v8当前申请的堆内存总大小
  heapUsed: 2857944, // 表示当前内存使用量
  external: 944391, // 表示v8内部的C++对象所占用的内存
  arrayBuffers: 9382
}
```
V8的垃圾回收策略主要是基于`分代式垃圾回收机制`，根据对象的存活时间将内存的垃圾回收进不同的分代，然后对不同的分代采用不同的垃圾回收算法。

- `新生代(new_space)`: 区域相对较小但垃圾回收特别频繁