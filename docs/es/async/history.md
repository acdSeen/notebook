# 异步发展历史

## 回调地狱

```javascript
let fs = require('fs');
fs.readFile('./template.txt', function (err, data) {
  if (err) {
    throw err;
  } else {
    fs.readFile(data, function (err, data) {
      console.log(data.toString()); // fqwfrqwwq
    });
  }
});
```

`不难想象，如果依次读取两个以上的文件，就会出现多重嵌套。代码不是纵向发展，而是横向发展，很快就会乱成一团，无法管理。因为多个异步操作形成了强耦合，只要有一个操作需要修改，它的上层回调函数和下层回调函数，可能都要跟着修改。这种情况就称为"回调函数地狱"（callback hell）。`

## Promise/Deferred 模式

```javascript
let fs = require('fs');
function readFile(filename) {
  return new Promise((resolve, reject) => {
    fs.readFile(filename, function (err, data) {
      if (err) {
        reject(err);
      } else {
        resolve(data);
      }
    });
  });
}
readFile('./template.txt')
  .then((data) => {
    return readFile(data.toString());
  })
  .then((data) => {
    console.log(data.toString());
  });
```

`Promise 的最大问题是代码冗余，原来的任务被 Promise 包装了一下，不管什么操作，一眼看去都是一堆 then，原来的语义变得很不清楚。`

## 生成器 Generators/yield

- 当你在执行一个函数的时候，你可以在某个点暂停函数的执行，并且做一些其他工作，然后再返回这个函数继续执行， 甚至是携带一些新的值，然后继续执行。
- 上面描述的场景正是 JavaScript 生成器函数所致力于解决的问题。当我们调用一个生成器函数的时候，它并不会立即执行， 而是需要我们手动的去执行迭代操作（next 方法）。也就是说，你调用生成器函数，它会返回给你一个迭代器。迭代器会遍历每个中断点。
- next 方法返回值的 value 属性，是 Generator 函数向外输出数据；next 方法还可以接受参数，这是向 Generator 函数体内输入数据

### Generator 的使用

```javascript
function* gen(x) {
  var y = yield x + 2;
  return y;
}
var g = gen(1);
g.next(); // { value: 3, done: false }
// 2传入 Generator 函数，作为上个阶段异步任务的返回结果，被函数体内的变量y接收
g.next(2); // { value: 2, done: true }
```

```javascript
let fs = require('fs');
function readFile(filename) {
  return new Promise((resolve, reject) => {
    fs.readFile(filename, function (err, data) {
      if (err) {
        reject(err);
      } else {
        resolve(data);
      }
    });
  });
}
function* read() {
  let nextUrl = yield readFile('./template.txt');
  let data = yield readFile(nextUrl);
  return nextUrl + '+' + data;
}
let r = read();
r.next()
  .value.then((data) => {
    return data.toString(); // tdata.txt
  })
  .then((data) => {
    return r.next(data).value
  })
  .then(data => {
    console.log(data.toString()) // fqwfrqwwq
  });
```

`虽然 Generator 函数将异步操作表示得很简洁，但是流程管理却不方便（即何时执行第一阶段、何时执行第二阶段）。`

## Co

`co 是一个为 Node.js 和浏览器打造的基于生成器的流程控制工具，借助于 Promise，你可以使用更加优雅的方式编写非阻塞代码`

```javascript
let fs = require('fs');
function readFile(filename) {
  return new Promise((resolve, reject) => {
    fs.readFile(filename, function (err, data) {
      if (err) {
        reject(err);
      } else {
        resolve(data);
      }
    });
  });
}
function* read() {
  let template = yield readFile('./template.txt');
  let data = yield readFile(template);
  return template + '+' + data;
}
function co(gen) {
  let it = gen();
  return new Promise((resolve, reject) => {
    (function next(lastVal) {
      let { value, done } = it.next(lastVal);
      if (done) {
        resolve(value);
      } else {
        value.then(next, (reason) => reject(reason));
      }
    })();
  });
}
co(read).then(
  function (data) {
    console.log(data); // tdata.txt+fqwfrqwwq
  },
  function (err) {
    console.log(err);
  }
);
```

## Async/await

- 内置执行器
- 更好的语义
- 更广的适用性

```javascript
let fs = require('fs');
function readFile(filename) {
  return new Promise((resolve, reject) => {
    fs.readFile(filename, function (err, data) {
      if (err) {
        reject(err);
      } else {
        resolve(data);
      }
    });
  });
}
async function read() {
  let template = await readFile('./template.txt');
  let data = await readFile(template);
  console.log(template + '+' + data); // tdata.txt+fqwfrqwwq
}
read();
```
