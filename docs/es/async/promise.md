# Promise 对象

> Promise 是异步编程的一种解决方法：
> 从语法上讲，promise 是一个对象，它可以获取异步操作的消息。从本意上讲，它是承诺，承诺过一段时间会给你一个结果。
> promise 有三种状态：**pending(等待态)，fulfiled(成功态)，rejected(失败态)**，状态一旦改变，就不会再变。创造 promise 实例后，它会立即执行。

## 编写 promiseA+规范的 promise 实现

再实现之前，可以先看一下[Promise A plus 规范](https://promisesaplus.com)

### 1.创建 promise 构造函数

---

先实现最基本功能：`promise创建后立即执行，在then时执行相应的函数，捕获错误立即变成reject态`。

```javascript
// promise里只有一个参数，叫executor（执行器）
function Promise(executor) {
  let self = this;
  self.status = 'pending'; // 等待态
  self.value = undefined; // 默认成功的值
  self.err = undefined; // 默认失败的值
  function resolve(value) {
    if (self.status === 'pending') {
      self.status = 'resolved';
      self.value = value;
    }
  }
  function reject(err) {
    if (self.status === 'pending') {
      self.status = 'rejected';
      self.err = err;
    }
  }
  // 捕获时发生异常，直接变成reject态，抛出错误
  try {
    executor(resolve, reject); // Promise实例创建后，立即执行executor
  } catch (error) {
    reject(error);
  }
}
// 定义then方法
Promise.prototype.then = function (onFulfilled, onRejected) {
  let self = this;
  if (self.status === 'resolved') {
    onFulfilled(self.value);
  }
  if (self.status === 'rejected') {
    onRejected(self.err);
  }
};
// 测试
let myPromise = new Promise((resolve, reject) => {
  console.log('start'); // executor立即执行
  resolve('resolved'); // status变成resolve
});
// status是resolved态。执行onFulfilled()
myPromise.then(
  (res) => {
    console.log(res);
  },
  (err) => {
    console.log(err);
  }
);
```

### 2.Promise 异步调用

---

异步代码不会立即执行，此时既不是 resolved 也不是 rejected，而是`pending`。所以需要在 then 里判断 status 为 pending 是，现将 onFulfilled，onRejected 存入数组里，当 status 改变是，再遍历数组让里面的函数依次执行。

1. 声明两个存放 onFulfiled(),onRejected()的数组

```javascript
function Promise(executor) {
  let self = this;
  self.status = 'pending';
  self.value = undefined;
  self.err = undefined;
  self.onResolvedCallbacks = []; // 存放then成功的回调
  self.onRejectedCallbacks = []; // 存放then失败的回调
  function resolve(value) {
    if (self.status === 'pending') {
      self.status = 'resolved';
      self.value = value;
      // 调用resolve时，依次执行onResolvedCallbacks里的函数
      self.onResolvedCallbacks.forEach((fn) => {
        fn();
      });
    }
  }
  function reject(err) {
    if (self.status === 'pending') {
      self.status = 'rejected';
      self.err = err;
      // 调用reject时，依次onRejectedCallbacks里的函数
      self.onRejectedCallbacks.forEach((fn) => {
        fn();
      });
    }
  }
  try {
    executor(resolve, reject);
  } catch {
    reject(error);
  }
}
```

2. 接着在 then 方法里添加 pending 的判断

```javascript
Promise.prototype.then = function (onFulfilled, onRejected) {
  let self = this;
  if (self.status === 'resolved') {
    onFulfilled(self.value);
  }
  if (self.status === 'rejected') {
    onRejected(self.err);
  }
  // 此时没resolved，也没rejectd
  if (self.status === 'pending') {
    self.onResolvedCallbacks.push(() => {
      onFulfilled(self.value);
    });
    self.onRejectedCallbacks.push(() => {
      onRejected(self.err);
    });
  }
};
// 测试
let myPromise = new Promise((resolve, reject) => {
  setTimeout(() => resolve('async'), 1000);
});
myPromise.then((res) => console.log(res)); // 1s后打印出async
```

### 3.Promise 链式调用

---

（1）规范里说在同一个 promise 里 then 可以被多次调用。

> ![](https://user-gold-cdn.xitu.io/2018/4/6/16299d0a93f79dfa?w=469&h=31&f=png&s=3953)

（2）jquery 能实现链式调用靠的是返回 this，而 promise 不能返回 this，规范里又说它返回的是一个新的 Promise 实例
**（注意，不是原来那个 Promise 实例）**；

> ![](https://user-gold-cdn.xitu.io/2018/4/6/16299d1293ff9ed9?w=446&h=37&f=png&s=3328)

在 then 里新建一个 promise2 并为每一个状态包一个 Promise

```javascript
Promise.prototype.then = function (onFulfilled, onRejected) {
  // 在 then 里新建一个 promise2 并为每一个状态包一个 Promise
  let promise2;
  let self = this;
  if (self.status === 'resolved') {
    promise2 = new Promise((resolve, reject) => {
      try {
        // 定义一个变量并接收上次then方法的返回值
        let x = onFulfilled(self.value);
        resolve(x); // 将返回值传入下次then的成功回调
      } catch (err) {
        reject(err);
      }
    });
    onFulfilled(self.value);
  }
  if (self.status === 'rejected') {
    promise2 = new Promise((resolve, reject) => {
      try {
        let x = onRejected(self.value);
        resolve(x);
      } catch (err) {
        reject(err);
      }
    });
  }
  if (self.status === 'pending') {
    self.onResolvedCallbacks.push(() => onFulfilled(self.value));
    self.onRejectedCallbacks.push(() => onRejected(self.err));
  }
  return promise2; // 返回新的promise
};
```

这里就返回了一个新的 promise2，在 promise2 里也需要调用 resolve 或者 reject；这里声明一个 **x** 来储存上一次 then 的返回值

**规范里说只要上一次 then 有返回值，下一次 then 一定调用成功态 resolve(x)**
再来看看规范，规范里说道

**（1）x 可能是一个 promise；**

> ![](https://user-gold-cdn.xitu.io/2018/4/6/16299d1d39665c71?w=361&h=32&f=png&s=3534)

**（2）可能是一个对象或者方法；**

> ![](https://user-gold-cdn.xitu.io/2018/4/6/16299d1fd36b4218?w=331&h=31&f=png&s=3144)

**（3）也有可能是一个普通的值**。

> ![](https://user-gold-cdn.xitu.io/2018/4/6/16299d2209511bbc?w=459&h=30&f=png&s=4059)

这时需要一个方法来处理 x

#### 3.1 对 onFulfilled 和 onRejected 的返回值进行处理

---

于是引入一个处理方法 resolvePromise(promise2, x, resolve, reject)；
这里四个参数分别是

1. Promise2 是我们返回的新的 promise
2. x 是上一次 then 的返回值
3. resolve 是成功的方法
4. reject 是失败的方法

**这里需要注意一下，有些人写的 promise 可能会既调用成功，又调用失败，如果两个都调用先调用谁另一个就忽略掉。**
在 resolvePromise(promise2, x, resolve, reject)里增加一个判断`called`表示是否调用过成功或者失败。

```javascript
function resolvePromise(promise2, x, resolve, reject) {
  // promise2和x不能相同
  if (promise2 === x) {
    return reject(new TypeError('循环引用了'));
  }
  let called; // 表示是否调用过成功或者失败
  // 这里对x的类型进行判断
  if (x !== null && (typeof x === 'object' || typeof x === 'function')) {
    try {
      // 判断x是不是promise，如果x是对象并且x的then方法是函数就认为他是一个promise
      let then = x.then;
      if (typeof then === 'function') {
        then.call(
          x,
          function (y) {
            if (called) return;
            called = true;
            // y可能还是一个promise，在去解析直到返回的是一个普通值
            resolvePromise(promise2, y, resolve, reject);
          },
          function (err) {
            if (called) return;
            called = true;
            reject(err);
          }
        );
      } else {
        resolve(x);
      }
    } catch (e) {
      if (called) return;
      called = true;
      reject(e);
    }
  } else {
    // x是一个普通值
    resolve(x);
  }
}
```

修改 then 方法

```javascript
Promise.prototype.then = function (onFulfilled, onRejected) {
  let promise2;
  let self = this;
  if (self.status === 'resolved') {
    promise2 = new Promise((resolve, reject) => {
      try {
        let x = onFulfilled(self.value);
        // resolve(x); // 将返回值传入下次then的成功回调
        resolvePromise(promise2, x, resolve, reject);
      } catch (err) {
        reject(err);
      }
    });
  }
  if (self.status === 'rejected') {
    promise2 = new Promise(function (resolve, reject) {
      try {
        let x = onRejected(self.err);
        resolvePromise(promise2, x, resolve, reject);
      } catch (e) {
        reject(e);
      }
    });
  }
  // 当调用then时可能没成功 也没失败
  if (self.status === 'pending') {
    promise2 = new Promise(function (resolve, reject) {
      self.onResolvedCallbacks.push(function () {
        try {
          let x = onFulfilled(self.value);
          resolvePromise(promise2, x, resolve, reject);
        } catch (e) {
          reject(e);
        }
      });
      self.onRejectedCallbacks.push(function () {
        try {
          let x = onRejected(self.err);
          resolvePromise(promise2, x, resolve, reject);
        } catch (e) {
          reject(e);
        }
      });
    });
  }
  return promise2; // 返回新的promise
};
```

这时我们就实现了 promise 的链式调用。

### 4.值的穿透问题

---

如果在 then 中什么都不传，值会穿透到最后调用的时候；
![](https://user-gold-cdn.xitu.io/2018/4/6/16299d295e308152?w=440&h=266&f=png&s=27704)

**这时需要在 then 里给 onFulfilled 和 onRejected 写一个默认的函数**

```javascript
onFulfilled =
  typeof onFulfilled === 'function'
    ? onFulfilled
    : function (value) {
        return value;
      };
onRejected =
  typeof onRejected === 'function'
    ? onRejected
    : function (err) {
        throw err; //这里需要抛出错误，不能return err，否则会在下一次调用成功态
      };
```

### 5. then 的异步实现

---

**规范里要求，所有的 onFulfilled 和 onRejected 都要确保异步执行**

> ![](https://user-gold-cdn.xitu.io/2018/4/6/16299d2c208125e6?w=792&h=155&f=png&s=28240)

这里以 resolve 为例，写一个 setTimeout():

```javascript
if (self.status === 'resolved') {
  promise2 = new Promise((resolve, reject) => {
    setTimeout(() => {
      try {
        // 定义一个变量并接收上次then方法的返回值
        let x = onFulfilled(self.value);
        resolvePromise(promise2, x, resolve, reject);
      } catch (err) {
        reject(err);
      }
    });
  });
}
```

### 6. defer 语法糖

---

在使用 promise 的过程中，我们都需要先 new Promise()，比如说：

```javascript
function read() {
  let fs = require('fs');
  let promise = new Promise(function (resolve, reject) {
    fs.readFile('./1.txt', 'utf8', function (err, data) {
      if (err) reject(err);
      resolve(data);
    });
  });
  return promise;
}
```

在 Promise 中，它为我们提供了一个语法糖**Promise.defer**，用 Promise.defer 只需这样写：

```javascript
function read() {
  let defer = Promise.defer();
  require('fs').readFile('.//1.txt', 'utf8', function (err, data) {
    if (err) defer.reject(err);
    defer.resolve(data);
  });
  return defer.promise;
}
```

为此，再为我们的 Promise 加一个 defer 方法：

```javascript
Promise.defer = Promise.deferred = function () {
  let dfd = {};
  dfd.promise = new Promise(function (resolve, reject) {
    dfd.resolve = resolve;
    dfd.reject = reject;
  });
  return dfd;
};
```

在这里，基本实现了一个比较完整的 promise；当然 Promise 还有许多静态方法，还有 js 的异步发展史。

`完整代码`

```javascript
// promise里只有一个参数，叫executor(执行器)
function Promise(executor) {
  let self = this;
  self.status = 'pending'; //等待态
  self.value = undefined; //默认成功的值
  self.err = undefined; //默认失败的值
  self.onResolvedCallbacks = []; // 存放then成功的回调
  self.onRejectedCallbacks = []; // 存放then失败的回调
  function resolve(value) {
    if (self.status === 'pending') {
      self.status = 'resolved';
      self.value = value;
      self.onResolvedCallbacks.forEach(function (fn) {
        fn();
      });
    }
  }
  function reject(err) {
    if (self.status === 'pending') {
      self.status = 'rejected';
      self.err = err;
      self.onRejectedCallbacks.forEach(function (fn) {
        fn();
      });
    }
  }
  try {
    //捕获时发生异常，直接变成reject态，抛出错误
    executor(resolve, reject); //promise实例创建后，立即执行
  } catch (error) {
    reject(error);
  }
}
function resolvePromise(promise2, x, resolve, reject) {
  if (promise2 === x) {
    //promise2和x不能相同
    return reject(new TypeError('循环引用了'));
  }
  let called; // 表示是否调用过成功或者失败
  //这里对x的类型进行判断
  if (x !== null && (typeof x === 'object' || typeof x === 'function')) {
    try {
      // 判断x是不是promise，如果x是对象并且x的then方法是函数我们就认为他是一个promise
      let then = x.then;
      if (typeof then === 'function') {
        then.call(
          x,
          function (y) {
            if (called) return;
            called = true;
            // y可能还是一个promise，在去解析直到返回的是一个普通值
            resolvePromise(promise2, y, resolve, reject);
          },
          function (err) {
            //失败
            if (called) return;
            called = true;
            reject(err);
          }
        );
      } else {
        resolve(x);
      }
    } catch (e) {
      if (called) return;
      called = true;
      reject(e);
    }
  } else {
    // 说明是一个普通值1
    resolve(x); // 表示成功了
  }
}
//在prototype上定义then实例方法
Promise.prototype.then = function (onFulfilled, onRejected) {
  onFulfilled =
    typeof onFulfilled === 'function'
      ? onFulfilled
      : function (value) {
          return value;
        };
  onRejected =
    typeof onRejected === 'function'
      ? onRejected
      : function (err) {
          throw err; //这里需要抛出错误，不能return err，否则会在下一次调用成功态
        };
  let self = this;
  let promise2; //返回的promise
  if (self.status === 'resolved') {
    promise2 = new Promise(function (resolve, reject) {
      setTimeout(function () {
        try {
          let x = onFulfilled(self.value);
          resolvePromise(promise2, x, resolve, reject);
        } catch (e) {
          reject(e);
        }
      });
    });
  }
  if (self.status === 'rejected') {
    promise2 = new Promise(function (resolve, reject) {
      setTimeout(function () {
        try {
          let x = onRejected(self.err);
          resolvePromise(promise2, x, resolve, reject);
        } catch (e) {
          reject(e);
        }
      });
    });
  }
  // 当调用then时可能没成功 也没失败
  if (self.status === 'pending') {
    promise2 = new Promise(function (resolve, reject) {
      // 此时没有resolve 也没有reject
      self.onResolvedCallbacks.push(function () {
        setTimeout(function () {
          try {
            let x = onFulfilled(self.value);
            resolvePromise(promise2, x, resolve, reject);
          } catch (e) {
            reject(e);
          }
        });
      });
      self.onRejectedCallbacks.push(function () {
        setTimeout(function () {
          try {
            let x = onRejected(self.err);
            resolvePromise(promise2, x, resolve, reject);
          } catch (e) {
            reject(e);
          }
        });
      });
    });
  }
  return promise2;
};
Promise.defer = Promise.deferred = function () {
  let dfd = {};
  dfd.promise = new Promise(function (resolve, reject) {
    dfd.resolve = resolve;
    dfd.reject = reject;
  });
  return dfd;
};

module.exports = Promise;
```
