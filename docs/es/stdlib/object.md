# Object 对象

## 概述

`Object`对象的原生方法分成两类：`Object`本身的方法（静态方法）与`Object`的实例方法。

## Object 的静态方法

### Object.keys()，Object.getOwnPropertyNames()

`Object.keys`方法和`Object.getOwnPropertyNames`方法都用来遍历对象的属性。

```javascript
var obj = { p1: 123, p2: 4778 };
Object.keys(obj); // ['p1','p2']
Object.getOwnPropertyNames(obj); // ['p1','p2']
```

`Object.keys`只返回可枚举的属性，`Object.getOwnPropertyNames`可返回不可枚举的属性。

```javascript
var obj = { p1: 123, p2: 4778 };
Object.defineProperty(obj, 'akk', {
  enumerable: false,
  value: '98126',
});
console.log(Object.keys(obj)); // ['p1','p2']
console.log(Object.getOwnPropertyNames(obj)); // ['p1','p2','akk']
```

**（1）对象属性模型的相关方法**

- `Object.getOwnPropertyDescriptor()`：获取某个属性的描述对象。
- `Object.defineProperty()`：通过描述对象，定义某个属性。
- `Object.defineProperties()`：通过描述对象，定义多个属性。

**（2）控制对象状态的方法**

- `Object.preventExtensions()`：防止对象扩展。
- `Object.isExtensible()`：判断对象是否可扩展。
- `Object.seal()`：禁止对象配置。
- `Object.isSealed()`：判断一个对象是否可配置。
- `Object.freeze()`：冻结一个对象。
- `Object.isFrozen()`：判断一个对象是否被冻结。

**（3）原型链相关方法**

- `Object.create()`：该方法可以指定原型对象和属性，返回一个新的对象。
- `Object.getPrototypeOf()`：获取对象的`Prototype`对象。

## Object 的实例方法

- `Object.prototype.valueOf()`：返回当前对象对应的值。
- `Object.prototype.toString()`：返回当前对象对应的字符串形式。
- `Object.prototype.toLocaleString()`：返回当前对象对应的本地字符串形式。
- `Object.prototype.hasOwnProperty()`：判断某个属性是否为当前对象自身的属性，还是继承自原型对象的属性。
- `Object.prototype.isPrototypeOf()`：判断当前对象是否为另一个对象的原型。
- `Object.prototype.propertyIsEnumerable()`：判断某个属性是否可枚举。