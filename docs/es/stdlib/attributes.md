# 属性描述对象

## 概述

属性描述对象提供了6个元属性
（1）`value`
`value`是该属性的属性值，默认为`undefined`。
（2）`writeable`
`writeable`是一个布尔值，表示属性值（value）是否可改变（即是否可写），默认为`true`。
（3）`enumerable`
`enumerable`是一个布尔值，表示该属性是否可遍历，默认为true
（4）`configurable`
`configurable`是一个布尔值，表示属性是否可配置，控制属性描述对象的可写性。
（5）`get`
`get`是一个函数，表示该属性的取值函数（getter），默认为`undefined`
（6）`set`
`set`是一个函数，表示该属性的存值函数（setter），默认为`undefined`

## Object.getOwnPropertyDescriptor()

`Object.getOwnPropertyDescriptor()`方法啊可以获取属性描述对象。参数为目标对象和字符串。

```javascript
var obj={p:'a'};
Object.getOwnPropertyDescriptor(obh,'p')
// Objeect {
//   value:'a',
//   writable:true,
//   enumerable:true,
//   configurable:true,
// }
```

注意，`Object.getOwnPropertyDescriptor()`方法只能用于对象自身的属性，不能用于继承的属性。

## Object.getOwnPropertyNames()

`Object.getOwnPropertyNames`方法返回一个数组，成员是参数对象自身的全部属性的属性名，不管该属性是否可遍历。
跟`Object.keys`的行为不同，`Object.keys`只返回对象自身的可遍历属性的全部属性名。

```javascript
Object.keys([]) // []
Object.getOwnPropertyNames([]) // [ 'length' ]

Object.keys(Object.prototype) // []
Object.getOwnPropertyNames(Object.prototype)
// ['hasOwnProperty',
//  'valueOf',
//  'constructor',
//  'toLocaleString',
//  'isPrototypeOf',
//  'propertyIsEnumerable',
//  'toString']
```

## Object.defineProperty()，Object.defineProperties()

`Object.defineProperty()`方法允许通过属性描述对象，定义或修改一个属性，返回修改后的对象。
```javascript
Object.deefineProperty(object,propertyName,attributesObject)
```
`Object.defineProperty`方法接受三个参数
- object：属性所在的对象
- propertyName：字符串，表示属性名
- attributesObject：属性描述对象

如果一次性定义或修改多个属性，可以使用`Object.defineProperties()`方法。

## 对象的拷贝

```javascript
var extend=function(to,from){
  for(var property in from ){
    to[property]=from[property]
  }
  return to;
}
extend({},{a:1})
```

上面这个方法的问题在于，如果遇到存取器定义的属性，会只拷贝值。

```javascript
extend({}, {
  get a() { return 1 }
})
// {a: 1}
```

为了解决这个问题，我们可以通过`Object.defineProperty`方法来拷贝属性。

```javascript
var extend=function(to,from){
  for(var property in form){
    if(!from.hasOwnProperty(property))continue;
    Object.defineProperty(
      to,
      property,
      Object.getOwnPropertyDescriptor(from,property)
    )
  }
}
extend({}, { get a(){ return 1 } })
// { get a(){ return 1 } })
```

上面代码中，`hasOwnProperty`那一行用来过滤掉继承的属性，否则可能会报错，因为`Object.getOwnPropertyDescriptor`读不到继承属性的属性描述对象。

## 控制对象状态

有时需要冻结对象的读写状态，防止对象被改变。JavaScript 提供了三种冻结方法，最弱的一种是`Object.preventExtensions`，其次是`Object.seal`，最强的是`Object.freeze`。

### Object.freeze()

`Object.freeze`方法可以使得一个对象无法添加新属性、无法删除旧属性、也无法改变属性的值，使得这个对象实际上变成了常量。

```javascript
var obj = {
  p: 'hello'
};

Object.freeze(obj);

obj.p = 'world';
obj.p // "hello"

obj.t = 'hello';
obj.t // undefined

delete obj.p // false
obj.p // "hello"
```