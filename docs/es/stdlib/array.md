# Array 对象

## 构造函数

`Array`是js原升对象，也是构造函数。

```javascript
new Array(1) // [empty]
new Aarray('abc') // ['abc']
new Array([1]) // [[1]]
```

```javascript
var arr=[2,1,3]
typeof arr // 'object'
Array.isArray(arr) // true
```

## 实例方法

### valueOf() toString()

`valueOf`方法是一个所有对象都拥有的方法，表示对该对象求值。不同对象的`valueOf`方法不一致。数组的`valurOf`方法返回数组本身。

```javascript
var arr=[1,2,3];
arr.valueOf() // [1,2,3]
```

`toString`也是通用的方法，数组的`toString`返回数组的字符串形式。

```javascript
var arr=[1,2,3,[3,4,5]]
arr.toString() // '1,2,3,3,4,5'
```

### join()

`join()`方法以指定参数作为分隔符，将所有数组成员连接为一个字符串返回，如果无参数，默认逗号分隔

```javascript
var a=[1,2,3,4];
a.join() // '1,2,3,4`
a.join('-') // '1-2-3-4'
Array.prototype.join.call('hello','-'); //h-e-l-l-o
```

### concat()

`concat`返回一个新数组并且返回的是当前数组的一个浅拷贝，`返回一个新数组`

```javascript
var obj={a:1};
var oldArr=[obj];
var newArr=oldArr.concat();
newArr[0].a //1
obj.a=2;
newArr[0].a //2
```

### reverse()

`reverse`方法用于颠倒排列数组，`会改变原数组`

### slice()

`slice`用于提取目标数组的一部分，`返回一个新数组`

```javascript
var a=['a','b','c'];
a.slice(1,2) // ['b']
```

`slice()`方法的一个重要应用，是将类似数组的对象转为真正的数组。

### splice()

`splice()`方法用于删除原数组的一部分成员，并可以在删除的位置添加新的成员，`会改变原数组`

```javascript
arr.splice(start, count, addElement1, addElement2, ...);
```

`splice`的第一个参数是删除的起始位置（从0开始），第二个参数是被删除的元素个数。如果后面还有更多的参数，则表示这些就是要被插入数组的新元素。

```javascript
var a = ['a', 'b', 'c', 'd', 'e', 'f'];
a.splice(4, 2, 1, 2) // ["e", "f"] 删除ef，插入12
a // ["a", "b", "c", "d", 1, 2]
```


### sort()

`sort`方法对数组成员进行排序，默认是按照字典顺序排序。排序后，原数组将被改变，`会改变原数组`。

```javascript
// bad
[1, 4, 2, 6, 0, 6, 2, 6].sort((a, b) => a > b)

// good
[1, 4, 2, 6, 0, 6, 2, 6].sort((a, b) => a - b)
```

上面代码中，前一种排序算法返回的是布尔值，这是不推荐使用的。后一种是数值，才是更好的写法。

### forEach() map()

如果数组遍历的目的是为了得到返回值，那么使用`map`方法，否则使用`forEach`方法。
`map`方法还可以接受第二个参数，用来绑定回调函数内部的`this`变量。

```javascript
var arr = ['a', 'b', 'c'];
[1, 2].map(function (e) {
  return this[e]; // this指向arr
}, arr)
// ['b', 'c']
```

上面代码通过`map`方法的第二个参数，将回调函数内部的`this`对象，指向`arr`数组。
`forEach`和`map`方法无法中断执行，总是会将所有成员遍历完。如果希望符合某种条件时，就中断遍历，要使用`for`循环。

### filter()

`filter`方法用于过滤数组成员，满足条件的成员`返回一个新数组`。

### some()，every()

这两个方法类似于断言（assert），返回一个布尔值。表示判断数组成员是否符合某种条件。

### reduce()，reduceRight()

`reduce`和`reduceright`方法依次处理数组的每个成员，最终累计为一个值。`reduce`是从左到右，`reduceRight`是从右到左。

```javascript
function add(prev, cur) {
  return prev + cur;
}

[].reduce(add)
// TypeError: Reduce of empty array with no initial value
[].reduce(add, 1)
// 1
```

会改变原数组的方法：`reverse（颠倒），splice（删除，替换），sort（排序），`
返回新数组的方法：`slice（提取），concat（拼接浅拷贝），filter（过滤），`