# 对象的继承

## 原型对象概述

### 构造函数的缺点

```javascript
function Cat(name,color){
  this.name=name;
  this.color=color;
  this.meow=funtion(){
    console.log('mm')
  }
}
var cat1=newCat('maox','white');
var cat2=newCat('maov','black');
cat1.meow===cat2.meow // false
```

通过构造函数为实例对象定义属性，虽然很方便，但是有一个缺点。同一个构造函数的多个实例之间，无法共享属性，从而造成对系统资源的浪费。
上面代码中，`cat1`和`cat2`是同一个构造函数的两个实例，它们都具有`meow`方法。由于`meow`方法是生成在每个实例对象上面，所以两个实例就生成了两次。也就是说，每新建一个实例，就会新建一个`meow`方法。这既没有必要，又浪费系统资源，因为所有`meow`方法都是同样的行为，完全应该共享。

这个问题的解决方法，就是 JavaScript 的原型对象（prototype）。

```javascript
function Cat(name,color){
  this.name=name;
  this.color=color;
}
Cat.prototype.meow=funtion(){
  console.log('mm')
}
var cat1=newCat('maox','white');
var cat2=newCat('maov','black');
cat1.meow===cat2.meow // true
```

原型对象的属性不是实例对象自身的属性。只要修改原型对象，变动就立刻会体现在**所有**实例对象上。

### 原型链

所有对象都有自己的原型对象（prototype）。一方面，任何一个对象，都可以充当其他对象的原型；另一方面，由于原型对象也是对象，所以它也有自己的原型。因此，就会形成一个“原型链”（prototype chain）：对象到原型，再到原型的原型……
所有对象的原型最终都可以上溯到`Object.prototype`，`Object.prototype`的原型是`null`。`null`没有任何属性和方法，也没有自己的原型。因此，原型链的尽头就是`null`。
```javascript
Object.getPrototypeOf(Object.prototype) // null
```
读取对象的某个属性时，JavaScript 引擎先寻找对象本身的属性，如果找不到，就到它的原型去找，如果还是找不到，就到原型的原型去找。如果直到最顶层的`Object.prototype`还是找不到，则返回`undefined`。如果对象自身和它的原型，都定义了一个同名属性，那么优先读取对象自身的属性，这叫做“覆盖”（overriding）。

### constructor 属性

`prototype`对象有一个`constructor`属性，默认指向`prototype`对象所在的构造函数。

```javascript
function P() {}
P.prototype.constructor === P // true
```

由于`constructor`属性定义在`prototype`对象上面，意味着可以被所有实例对象继承。

```javascript
function P() {}
var p = new P();

p.constructor === P // true
p.constructor === P.prototype.constructor // true
p.hasOwnProperty('constructor') // false
```

上面代码中，`p`是构造函数`P`的实例对象，但是`p`自身没有`constructor`属性，该属性其实是读取原型链上面的`P.prototype.constructor`属性。

`constructor`属性的作用是，可以得知某个实例对象，到底是哪一个构造函数产生的。

```javascript
function F(){};
var f=new F();
f.constructor===F // true
f.constructor===RegExp // false
```

上面代码中，`constructor`属性确定了实例对象`f`的构造函数是`F`，而不是`RegExp`。
另一个方面，有了`constructor`属性，就可以从一个实例对象新建另一个实例对象

```javascript
function Constr(){}
var x=new Constr();
var y=new x.constructor();
y instanceof Constr // true
```
`constructor`属性表示原型对象与构造函数之间的关联关系，如果修改了原型对象，一般会同时修改`constructor`属性，防止引用的时候出错。

```javascript
// 坏的写法
C.prototype = {
  method1: function (...) { ... },
  // ...
};

// 好的写法
C.prototype = {
  constructor: C,
  method1: function (...) { ... },
  // ...
};

// 更好的写法
C.prototype.method1 = function (...) { ... };
```

## instanceof 运算符

`instanceof`运算符返回一个布尔值，表示对象是否为某个构造函数的实例。
`instanceof`运算符的左边是实例对象，右边是构造函数。它会检查右边构建函数的原型对象（prototype），是否在左边对象的原型链上。因此，下面两种写法是等价的。

```javascript
v instanceof Vehicle
// 等同于
Vehicle.prototype.isPrototypeOf(v)
```

注意，`instanceof`运算符只能用于对象，不适用原始类型的值。

```javascript
var s = 'hello';
s instanceof String // false
```

利用`instanceof`运算符解决调用构造函数时，忘了加`new`命令的问题

```javascript
function Fubar(foo,bar){
  if(this instanceof Fubar){
    this._foo=foo;
    this._bar=bar;
  }else{
    return new Fubar(foo,bar);
  }
}
```

## 构造函数的继承

```javascript
function Sub(val){
  // 调用父类的构造函数
  Super.call(this); 
  this.prop=val;
}
// 子类的原型指向父类的原型
Sub.prototype=Object.create(Super.prototype); 
Sub.prototype.constructor=Sub;

```

## 多重继承

JavaScript 不提供多重继承功能，即不允许一个对象同时继承多个对象。变通方法

```javascript
function M1(){
  this.hello='hello'
}
function M2(){
  this.world='world'
}
function S(){
  M1.call(this);
  M2.call(this);
}
// 继承M1
S.prototype=Object.create(M1.prototype);
// 继承M2
Object.assign(S.prototype,M2.prototype);
// 指定构造函数
S.prototypee.constructor=S;
var s=new S();
s.hello // 'hello'
s.world // 'world'
```

上面代码中，子类`S`同时继承了父类`M1`和`M2`。这种模式又称为 Mixin（混入）。

## 模块

### 基本的实现方法

```javascript
var module1=new Object({
  _count:0,
  m1:function(){},
  m2:function(){}
});
// m1和m2封装在module1对象里，使用时，调用这个对象的属性
module1.m1();
// 这样会暴露所有模块成员，内部状态可以被外部改写
module1._count=5
```

### 封装私有变量：构造函数的写法

```javascript
function StringBuilder(){
  var buffer=[];
  this.add=function (arr){
    buffer.push(str)
  }
  this.toString=function(){
    return buffer.join('')
  }
}

```

`buffer`是模块的私有变量。一旦生成实例对象，外部是无法直接访问`buffer`的。这意味着，构造函数有双重作用，既用来塑造实例对象，又用来保存实例对象的数据，违背了构造函数与实例对象在数据上相分离的原则

```javascript
function StringBuilder(){
  this._buffer=[];
}
StringBuilder.prototype={
  constructor:StringBuilder,
  add:function(str){
    this._buffer.push(str);
  },
  toString:function(){
    return this._buffer.join('');
  }
}
```

这种方法将私有变量放入实例对象中，好处是看上去更自然，但是它的私有变量可以从外部读写，不是很安全。

### 封装私有变量：立即执行函数的写法

```javascript
var module1 = (function () {
　var _count = 0;
　var m1 = function () {
　  //...
　};
　var m2 = function () {
　　//...
　};
　return {
　　m1 : m1,
　　m2 : m2
　};
})();
```

### 模块的放大模式

```javascript
var module1=(function(mod){
  mod.m3=function(){}
  return mod
})(module1)
```

在浏览器环境中，模块的各个部分通常都是从网上获取的，有时无法知道哪个部分会先加载。如果采用上面的写法，第一个执行的部分有可能加载一个不存在空对象，这时就要采用"宽放大模式"（Loose augmentation）

```javascript
var module1 = (function (mod) {
　//...
　return mod;
})(window.module1 || {});
```

与"放大模式"相比，“宽放大模式”就是“立即执行函数”的参数可以是空对象。

### 输入全局变量

```javascript
var module1 = (function ($, YAHOO) {
　//...
})(jQuery, YAHOO);
```

保证模块的独立性，还使得模块之间的依赖关系变得明显。

```javascript
(function($, window, document) {

  function go(num) {
  }

  function handleEvents() {
  }

  function initialize() {
  }

  function dieCarouselDie() {
  }

  //attach to the global scope
  window.finalCarousel = {
    init : initialize,
    destroy : dieCarouselDie
  }

})( jQuery, window, document );
```

上面代码中，`finalCarousel`对象输出到全局，对外暴露`init`和`destroy`接口，内部方法`go`、`handleEvents`、`initialize`、`dieCarouselDie`都是外部无法调用的。