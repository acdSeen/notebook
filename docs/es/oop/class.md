# Class（类）

## 概述

- 类的所有方法都定义在类的`prototype`属性上面。
- 类的数据类型是函数，类本身指向构造函数
- 类内部定义的方法都是不可枚举的（与 es5 不一致）
- 不存在函数提升（与 es5 不一致）

```javascript
class Point {
  constructor() {}
  getValue() {} // 等同于Point.prototype.getValue
}
typeof Point; //function
Point === Point.prototype.constructor; //true
// 类的数据类型是函数，类本身指向构造函数
Object.keys(Point.prototype); // []
Object.getOwnPropertyNames(Point.prototype); // ['constructor','getValue']
```

## 静态方法

类相当于实例的原型，所有在类中定义的方法，都会被实例继承。加上`static`，不会被继承，只能通过类来调用。

```javascript
class Foo {
  static classMethod() {
    return 'hello';
  }
}
Foo.classMethod(); // 'hello'
```

## 实例属性

```javascript
class Foo {
  // _count = 0; // 新提案 所有实例对象自身的属性都定义在类的头部，比较整齐
  constructor(name) {
    this._count = 0;
    this.name = name;
  }
  increment() {
    this._count++;
  }
}
let f = new Foo('Bob');
f.increment();
console.log(f.name, f._count);
```

## 静态属性

ES6 明确规定，Class 内部只有静态方法，没有静态属性。新提案加上`static`

```javascript
class MyClass {
  // static myStaticProp = 42; // 新提案
  constructor() {
    // console.log(MyClass.myStaticProp); // 42
  }
}
MyClass.myStaticProp = 42; // 现写法
```

## 继承

通过`extends`关键字实现继承，父类的静态方法，也会被子类继承。

- super()只能用在子类的构造函数之中
- super作为对象时，定义在父类实例上的方法或属性，是无法通过super调用的。
- super在静态方法之中指向父类，在普通方法之中指向父类的原型对象

```javascript
class Point {
  constructor(x, y) {
    this.x = x;
    this.y = y;
  }
  toString () {
    return `x:${this.x},y:${this.y}`
  }
  static toString (val) {
    console.log(val+1)
  }
}
class ColorPoint extends Point {
  constructor(x, y, color) {
    super(x, y); // 调用父类的constructor(x, y)
    this.color = color;
  }
  toString () {
    // console.log(super.x); // 定义在父类实例上的方法或属性，是无法通过super调用的。
    // 子类普通方法中通过super调用父类的方法时，方法内部的this指向当前的子类实例
    console.log(`${this.color} ${super.toString()}`);
  }
  static toString (val) {
    // 子类静态方法中调用父类，super指向父类而不是父类的原型对象
    super.toString(val) // 父类的静态方法toString
  }
}
ColorPoint.toString(100); // 101
let child = new ColorPoint(10, 20, '#fff')
child.toString(); // 10 20 #fff x:10,y:20
```

## 类的 prototype 属性和__proto__属性

1. 子类的`__proto__`属性，表示构造函数的继承，总是指向父类。
2. 子类`prototype`属性的`__proto__`属性，表示方法的继承，总是指向父类的`prototype`属性。
3. 子类实例的__proto__属性的__proto__属性，指向父类实例的__proto__属性，子类实例指向子类原型，子类的原型指向父类的原型

```javascript
var p1 = new Super();
var p2 = new Sub();
p2.__proto__ === p1.__proto__ // false
p2.__proto__.__proto__ === p1.__proto__ // true
```