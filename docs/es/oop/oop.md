# 面向对象

ECMA 把对象定义为`无序属性的集合，其属性可以包含基本值，对象或者函数`。

## 属性类型

ECMA 定义只有内部才用的特性(attibute)时，描述了属性(property)的各种特征。特性是内部值，它们放在两队儿方括号中，例如[[Enumerable]]。

### 数据属性

1. [[Configurable]]:表示能否通过 delete 删除属性，能否修改属性特性，能否把属性修改为访问器属性。默认 true
1. [[Enumerable]]:表示能否通过 for-in 遍历属性。默认 true
1. [[Writable]]:表示能否修改属性的值。默认 true
1. [[Value]]:属性的值。默认 undefined

### 访问器属性

1. [[Configurable]]:
1. [[Enumerable]]:
1. [[Get]]:读取属性时调用的函数。默认 undefined
1. [[Set]]:写入属性时调用的函数。默认 undefined

```javascript
var obj = { p1: 123, p2: 4778 };
Object.defineProperty(obj, 'akk', {
  enumerable: false,
  value: '98126',
});
for (const key in obj) {
  console.log(key); // p1,p2
}
console.log(Object.keys(obj)); // [ 'p1', 'p2' ]
console.log(Object.getOwnPropertyNames(obj)); // [ 'p1', 'p2', 'akk' ]
```

`Object.keys`和`for in`只能拿到可枚举的属性，`Object.getOwnPropertyNames`可以拿到不能枚举的属性

## 创建对象

### 工厂模式

```javascript
function createPerson(name, age) {
  var o = new Object();
  o.naem = name;
  o.age = age;
  o.sayName = function () {
    console.log(this.name);
  };
  return o;
}
var person1 = createPerson('Koas', 20);
var person2 = createPerson('Bob', 29);
```

`工厂模式解决创建多个相似对象的问题，但无法解决对象属于哪个类型的识别问题。`

### 构造函数

`相比工厂函数构造函数可以检测对象的类型`,`instanceof`运算符的左边是实例对象，右边是构造函数。它会检查右边构建函数的原型对象（prototype），是否在左边对象的原型链上

```javascript
function Person(name, age) {
  this.name = name;
  this.age = age;
  this.sayName = function () {
    console.log(this.name);
  };
}
var person1 = new Person('Koas', 20);
var person2 = new Person('Bob', 29);
console.log(person1 instanceof Person); // true
```

Person()和 createPerson()有不同之处

1. 没有显示的创建对象
1. 直接将属性赋给了 this
1. 没有 return

new 经历 4 个步骤

1. 创建一个新对象
1. this 指向新对象
1. 执行构造函数
1. 返回新对象

```javascript
function _new() {
  var params = Array.prototype.slice.call(arguments);
  var Parent = params.shift();
  var obj = Object.create(Parent.prototype);
  Parent.apply(obj, params);
  return obj;
}
```

```javascript
person1 instanceof Person; // true
```

`构造函数缺点是同一个构造函数的多个实例之间，无法共享属性，从而造成对系统资源的浪费`，比如每创建一个实例就会创建一个同样的方法。

### 原型模式

`将属性方法放在原型上就可以共享属性，避免资源浪费。`
每创建一个新函数，就会自动为该函数创建一个 prototype 属性。所有对象实例共享原型包含的属性和方法。所有的原型对象都有一个 constructor（构造函数属性），该属性是指向 prototype 属性所在函数的指针，如 constructor 指向 Person.

```javascript
function Person() {}
Person.prototype.name='Bon';
Person.prototype.sayName = function () {
  console.log(this.name);
};
```

当调用构造函数创建新实例后，实例内部包含一个指针[[Prototype]]（内部属性）无标准方式访问，指向构造函数的原型对象。
Firefox、Safari 和 Chrome 在每个对象上都支持一个属性`__proto__`，这个链接是存在于实例和构造函数的原型之间的。

[[prototype]]无法访问，但可以通过 isPrototypeOf()方法来确定对象之间是否存在这种关系。

```javascript
// [[Prototype]]指向调用isPrototypeOf()方法的对象(Person.prototype)
Person.prototype.isPrototypeOf(person1); // true
```

`Object.getPrototypeOf()`这个方法可以获取对象的原型。

`person1.hasOwnProperty()`方法可以检测属性是存在于实例中还是原型中。

#### 重写原型

使用对象字面量方式重写整个原型并保留 constructor 属性

```javascript
Person.prototype = {
  // 这样会使constructor的[[Enumerable]]为ture
  // constructor: Person,
  name: 'default',
  age: 28,
  sayName: function () {
    console.log(this.name);
  },
};
Object.defineProperty(Person.prototype, constructor, {
  enumerable: false,
  value: Person,
});
```

`实例中的[[prototype]]指针仅指向原型，不指向构造函数`，重写原型对象切断了现有原型与任何之前已经存在的对象实例之间的联系，它们引用的仍然是最初的原型。

```javascript
function Person() {}
var person1 = new Person();
Person.prototype = {
  constructor: Person,
  name: 'default',
  age: 28,
  sayName: function () {
    console.log(this.name);
  },
};
person1.sayName(); // error
var person2 = new Person();
person2.sayName(); // default
```

### 构造函数和原型组合模式

```javascript
function Person(name, age, job) {
  this.name = name;
  this.age = age;
  this.job = job;
  this.friends = ['Shelby', 'Court'];
}
Person.prototype = {
  constructor: Person,
  sayName: function () {
    alert(this.name);
  },
};
```

### 动态原型模式

```javascript
function Person(name, age) {
  this.name = name;
  this.age = age;
  if (typeof this.sayName !== 'function') {
    Person.prototype.sayName = function () {
      console.log(this.name);
    };
  }
}
```

### 寄生构造函数模式

寄生（parasitic）构造函数模式是创建一个函数，该函数仅是封装创建对象的代码，然后返回新创建的对象。

```javascript
function Person(name, age) {
  var obj = new Object();
  obj.name = name;
  obj.age = age;
  obj.sayName = function () {
    console.log(this.name);
  };
  return obj;
}
```

`这个模式可以在特殊的情况下用来为对象创建构造函数。假设我们想创建一个具有额外方法的特殊数组。由于不能直接修改 Array 原生构造函数，因此可以使用这个模式。`

```javascript
function MyArray() {
  var arr = new Array();
  arr.push.apply(arr, arguments);
  arr.toJoin = function (str) {
    return arr.join(str);
  };
  return arr;
}
var arr1 = new MyArray(1, 2, 3, 4);
console.log(arr1.toJoin('|')); // 1|2|3|4
console.log(arr1 instanceof MyArray); // false
console.log(arr1 instanceof Array); // true
```

寄生构造函数模式返回的对象与构造函数的原型没有关系，因此不能使用 instanceof 确定对象类型。

### 稳妥构造函数模式

`所谓稳妥对象，指的是没有公共属性，而且其方法也不引用 this 的对象`。稳妥对象最适合在一些安全的环境中（这些环境中会禁止使用 this 和 new），或者在防止数据被其他应用程序（如 Mashup 程序）改动时使用。稳妥构造函数遵循与寄生构造函数类似的模式，但有两点不同：`一是新创建对象的实例方法不引用 this；二是不使用 new 操作符调用构造函数。`

```javascript
function Person(name, age) {
  var o = new Object();
  // 没有公共属性
  o.sayName = function () {
    console.log(name); // 不使用this
  };
  return o;
}
var person1 = Person('Nob', 20); // 不用new
person1.sayName();
```

## 继承

继承方式：接口继承和实现继承。接口继承只继承方法签名，实现继承则继承实际的方法。由于函数没有签名，因此 ECMAscript 只支持实现继承，且实现继承主要依靠原型链来实现。

### 原型继承

构造函数有一个 prototype 对象，prototype 对象有个 constructor 指针指向构造函数，每个实例都包含一个指向原型对象的内部指针。如果`A.prototype=new B();B.prototype=new C();A.prototype.__proto__ -> B.prototype.__proto__ -> C.prototype`。

```javascript
function A() {}
function B() {}
function C() {
  this.name = 'Box';
}
C.prototype.sayName = function () {
  console.log(this.name);
};
B.prototype = new C();
A.prototype = new B();
var aoa = new A();
aoa.sayName(); // Box
```

1. B 原型换成了 C 的实例，B 的原型就有了 C 实例拥有的全部属性和方法，还有一个指向 C 原型的指针
2. A 原型等于 B 的实例，A 原型拥有指向 B 原型的指针
3. aoa 是 A 的实例，调用 sayName 函数：`1.先找实例本身，2.再通过[[prototype]]指针找到A的原型，3.A的原型里没有顺着指针找B的原型，4.B的原型没有顺着指针找C的原型。`
4. 寻找 this.name 的流程同上

**子类型给原型添加方法的代码一定要放在替换原型的语句之后。**

#### 原型继承的问题

1. 包含引用类型的原型属性会被所有实例共享，所以引用类型属性定义在构造函数中，而不是原型中。`原型继承中，原型会变成实例，而定义在实例中的引用类型属性也变成了原型属性`

```javascript
function SuperType() {
  this.hobbys = ['game', 'photo', 'draw'];
}
SuperType.prototype.showHobby = function () {
  console.log(this.hobbys);
};
var type1 = new SuperType();
type1.hobbys.push('football');
var type2 = new SuperType();
type1.showHobby(); // [ 'game', 'photo', 'draw', 'football' ]
// 修改构造函数里的引用类型属性不会影响其他实例
type2.showHobby(); // [ 'game', 'photo', 'draw' ]
function SubType() {}
// hobbys变成原型上的引用类型属性
SubType.prototype = new SuperType();
var type3 = new SubType();
type3.hobbys.push('football');
var type4 = new SubType();
type3.showHobby(); // [ 'game', 'photo', 'draw', 'football' ]
// 修改原型上的引用类型属性会影响所有实例
type4.showHobby(); // [ 'game', 'photo', 'draw', 'football' ]
```

2. 原型继承的第二个问题是：在创建子类型的实例时，不能向超类型的构造函数中传递参数。实际上，应该说是没有办法在不影响所有对象实例的情况下，给超类型的构造函数传递参数。

### 借用构造函数继承

借用构造函数（constructor stealing）也叫伪造对象或经典继承。即在子类型构造函数的内部调用超类型构造函数。

1. 解决原型继承的引用类型属性问题

```javascript
function SuperType() {
  this.hobbys = ['game', 'photo', 'draw'];
}
function SubType() {
  SuperType.call(this); // 借用构造函数
}
var type1 = new SubType();
type1.hobbys.push('football');
var type2 = new SubType();
console.log(type1.hobbys); // [ 'game', 'photo', 'draw', 'football' ]
console.log(type2.hobbys); // [ 'game', 'photo', 'draw' ]
```

2. 可以传递参数

```javascript
function SuperType(name) {
  this.name = name;
}
function SubType(name) {
  SuperType.call(this, name);
}
var type1 = new SubType('Bob');
console.log(type1.name); // Bob
```

`仅用借用构造函数继承的话，方法都只能定义在构造函数中，无法实现函数复用。`

### 组合继承

组合继承（combination inheritance），将原型继承和借用构造函数继承组合起来的继承。

```javascript
function SuperType(name, age) {
  this.name = name;
  this.age = age;
  this.hobbys = ['game', 'photo', 'draw'];
}
SuperType.prototype.showInfo = function () {
  console.log(this.hobbys, this.name, this.age);
};
function SubType(name, age) {
  // 借用构造函数继承
  SuperType.call(this, name, age);
}
// 原型继承
SubType.prototype = new SuperType();
var type1 = new SubType('Aoa', 20);
type1.hobbys.push('football');
var type2 = new SubType('Bob', 35);
type1.showInfo();
// [ 'game', 'photo', 'draw', 'football' ] 'Aoa' 20
type2.showInfo();
// [ 'game', 'photo', 'draw' ] 'Bob' 35
```

`组合继承避免了原型链和借用构造函数的缺陷，融合了它们的优点，成为 JavaScript中最常用的继承模式。而且，instanceof 和 isPrototypeOf()也能够用于识别基于组合继承创建的对象。`

### 原型式继承

借助原型可以基于已有的对象创建新对象，同时还不必因此创建自定义类型。

```javascript
function object(o) {
  function F() {}
  F.prototype = o;
  return new F();
}
```

本质上讲 object()对传入的对象执行了一次浅复制。

```javascript
var person = {
  name: 'undef',
  hobbys: ['game', 'photo', 'draw'],
};
function object(o) {
  function F() {}
  F.prototype = o;
  return new F(); 
}
var bob = object(person);
bob.name = 'Bob';
bob.hobbys.push('football');
console.log(bob.hobbys, bob.name);
// [ 'game', 'photo', 'draw', 'football' ] 'Bob'
console.log(person.hobbys, person.name);
// [ 'game', 'photo', 'draw', 'football' ] 'undef'
```

原型式继承要求必须有一个对象可以作为另一个对象的基础。person 是另一个对象的基础，并作为 F 对象的原型，hobbys 就变成了原型上的引用类型属性，会被所有子对象共享。
ES5 新增了`Object.create()`方法规范了原型式继承。

### 寄生式继承

寄生（parasitic）式继承的思路与寄生构造函数和工厂模式类似，即创建一个仅用于封装继承过程的函数，该函数在内部以某种方式来增强对象，最后再像真地是它做了所有工作一样返回对象。

```javascript
function creaateAnother(origin) {
  var clone = Object.create(origin); // 调用函数创建新对象
  clone.sayHi = function () { // 增强新对象
    console.log('hi');
  };
  return clone; // 返回这个对象
}
```

使用寄生式继承来为对象添加函数，会由于不能做到函数复用而降低效率；这一点与构造函数模式类似。 

### 寄生组合式继承

前面说组合继承是js最常用的继承，但是`无论如何都会调用两次超类型构造函数`：一次是创建子类型原型调用，一次是在子类型构造函数内部调用。

```javascript
function SurperType () {
  this.name = 'Aoa';
  this.age = 20;
}
function SubType () {
  // 第二次调用，又生成了实例属性name和age属性屏蔽了原型中的name和age
  SurperType.call(this); 
}
// 第一次调用，生成了name和age属性，存在prototype中
SubType.prototype = new SurperType(); 
var type1 = new SubType();
```

`有两组 name 和 colors 属性：一组在实例上，一组在 SubType 原型中。`解决这个问题方法——`寄生组合式继承`。
寄生组合式继承，即通过借用构造函数来继承属性，通过原型链的混成形式来继承方法。不必为了指定子类型的原型而调用超类型的构造函数，我们所需要的无非就是超类型原型的一个副本而已。本质上，就是使用寄生式继承来继承超类型的原型，然后再将结果指定给子类型的原型。

```javascript
function inherit (subtype, supertype) {
  var prototype = Object.create(supertype.prototype);
  prototype.constructor = subtype; // 弥补因重写原型而失去的默认的constructor属性
  subtype.prototype = prototype;
}
function SurperType (name) {
  this.name = name;
}
SurperType.prototype.showName = function () {
  Object.keys(this).map(key => {
    if (typeof this[key] !== 'function') {
      console.log(this[key])
    }
  })
}
function SubType (name, age) {
  SurperType.call(this, name);
  this.age = age;
}
inherit(SubType, SurperType);
var type1 = new SubType('Bob', 30);
type1.showName(); // Bob 30
```

这个例子的高效率体现在它只调用了一次 SuperType 构造函数，并且因此避免了在 SubType. prototype 上面创建不必要的、多余的属性。与此同时，原型链还能保持不变；因此，还能够正常使用instanceof 和isPrototypeOf()。开发人员普遍认为寄生组合式继承是引用类型最理想的继承范式。

## 总结

创建
- `工厂模式`（无法解决实例归属问题）
- `构造函数模式`（无法复用函数，造成资源浪费）
- `原型模式`（修改引用类型的原型属性会原型所有实例）
- `构造函数原型组合模式`
- `动态原型模式`（解决原型和构造函数分开写的问题）
- `寄生构造函数模式`（创建有原生对象属性且拥有额外方法的特殊构造函数）
- `稳妥构造函数模式`（没有公共属性，也不引用this，用于一些安全环境）

继承
- `原型继承`（原型中会有引用类型属性，不能像超类型传递参数）
- `借用构造函数继承`（方法定义在构造函数中，无法复用）
- `原型和借用构造函数组合继承`（超类型构造函数调用两次，属性生成两次实例上和原型中）
- `原型式继承`（对象浅拷贝）
- `寄生式继承` （函数不能复用）
- `寄生组合式继承` （解决调用两次的问题）