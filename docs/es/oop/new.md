# 实例对象与 new 命令

## new 命令

### new 命令的原理

1. 创建一个空对象，作为将要返回的对象实例。
1. 将这个空对象的原型，指向构造函数的`prototype`属性。
1. 将这个空对象赋值给函数内部的`this`关键字。
1. 开始执行构造函数内部的代码。

也就是说，构造函数内部，`this`指的是一个新生成的空对象，所有针对`this`的操作，都会发生在这个空对象上。构造函数之所以叫构造函数，就是说这个函数的目的，就是操作一个空对象（即`this`对象），将其构造成需要的样子。

```javascript
function Person(name,age){
  this.name=name;
  this.age=age;
  return this
}
function _new(constructor,params){
  // 将arguments转为数组
  var args=[].slice.call(arguments);
  // 取出构造函数
  var constructor=args.shift();
  // 创建一个空对象，继承构造函数的prototype属性
  var context=Object.create(constructor.prototype);
  // 执行构造函数
  var result=constructor.apply(context,args);
  // 如果返回结果是对象，就直接返回，否则返回context对象
  return (typeof result === 'object' && result != null) ? result : context;
}
var actor=_new(Person,'bob',24);
```

## Object.create() 创建实例对象

构造函数作为模板，可以生成实例对象。但是，有时拿不到构造函数，只能拿到一个现有的对象。我们希望以这个现有的对象作为模板，生成新的实例对象，这时就可以使用`Object.create()`方法。

```javascript
var person1 = {
  name: '张三',
  age: 38,
  greeting: function() {
    console.log('Hi! I\'m ' + this.name + '.');
  }
};

var person2 = Object.create(person1);

person2.name // 张三
person2.greeting() // Hi! I'm 张三.
```

上面代码中，对象`person1`是`person2`的模板，后者继承了前者的属性和方法。

`Object.create`方法生成的新对象，动态继承了原型。在原型上添加或修改任何方法，会立刻反映在新对象之上。

```javascript
var obj1 = { p: 1 };
var obj2 = Object.create(obj1);

obj1.p = 2;
obj2.p // 2
```
