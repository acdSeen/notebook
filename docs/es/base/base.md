## 防抖

防抖的原理就是：你尽管触发事件，但是我一定在事件触发 n 秒后才执行，如果你在一个事件触发的 n 秒内又触发了这个事件，`那我就以新的事件的时间为准`，n 秒后才执行，总之，就是要等你触发完事件 n 秒内不再触发事件，我才执行。

```javascript
function debounce(func, time) {
  let timer = null;
  return () => {
    clearTimeout(timer);
    timer = setTimeout(() => {
      func.apply(this, arguments);
    }, time);
  };
}
```

## 节流

节流的原理就是：
如果你持续触发事件，每隔一段时间，只执行一次事件。
根据首次是否执行以及结束后是否执行，效果有所不同，实现的方式也有所不同。
我们用 leading 代表首次是否执行，trailing 代表结束后是否再执行一次。
关于节流的实现，有两种主流的实现方式，一种是使用时间戳，一种是设置定时器。

```javascript
function throtte(func, time) {
  let activeTime = 0;
  return () => {
    const current = Date.now();
    if (current - activeTime > time) {
      func.apply(this, arguments);
      activeTime = Date.now();
    }
  };
}
```

## 模块化发展

模块化主要用来抽离公共代码，隔离作用域，避免变量冲突等。

IIFE：使用自执行函数，`在一个单独的函数作用域中执行代码，避免变量冲突`。

```javascript
(function () {
  return {
    data: [],
  };
})();
```

AMD：使用 requireJS，`依赖必须提前声明好`。

```javascript
define('./index.js', function (code) {});
```

CMD：使用 seaJS，`支持动态引入依赖文件`。

```javascript
define(function (require, exports, module) {
  var indexCode = require('./index.js');
});
```

CommonJS：node 中自带的模块化

```javascript
var fs = require('fs');
```

UMD：兼容 AMD，CommonJS 模块化语法。

ES Modules：ES6 引入的模块化

```javascript
import a from 'a';
```

## 箭头函数与普通函数

1. 箭头函数的 this 对象，是定义时所在的对象，不是使用时所在的对象。
2. 箭头函数内不存在 arguments 对象，如果要用，可以用 rest 参数代替。
3. 不能使用 yield 命令，因此箭头函数不能用作 Generator 函数。
4. 不可以使用 new 命令，因为
   · 没有自己的 this，无法调用 call，apply。
   · 没有 prototype 属性，new 在执行时需要将构造函数的 prototype 赋值给新的对象的**proto**

