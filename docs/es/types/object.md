# 对象

## 属性的操作

### 属性是否存在：in 运算符

`in`运算符用于检查对象是否包含某个属性，它不能识别哪些属性是对象自身的，哪些属性是继承的。

```javascript
var obj={p:1};
'p' in obj // true
'toString' in obj // true
```

这时，可以使用对象的`hasOwnProperty`方法判断一下，是否为对象自身的属性。

```javascript
var obj={};
if('toString' in obj){
  console.log(obj.hasOwnProperty('toString')) // false
}
```

### 属性的遍历：for...in 循环

`for...in`循环用来遍历一个对象的全部属性。

`for...in`循环有两个使用注意点。

- 它遍历的是对象所有可遍历（enumerable）的属性，会跳过不可遍历的属性。
- 它不仅遍历对象自身的属性，还遍历继承的属性。使用`for...in`的时候，应该结合使用`hasOwnProperty`方法，在循环内部判断一下，某个属性是否为对象自身的属性。

```javascript
var person={name:'zbo'};
for (var i in person){
  if(person.hasOwnProperty(i)){
    console.log(person[i])
  }
}
```

### 闭包（closure）

JavaScript 语言特有的"链式作用域"结构（chain scope），子对象会一级一级地向上寻找所有父对象的变量。所以，父对象的所有变量，对子对象都是可见的，反之则不成立。

闭包的最大用处有两个，一个是可以读取函数内部的变量，另一个就是让这些变量始终保持在内存中。

闭包的另一个用处，是封装对象的私有属性和私有方法。

```javascript
function Person(name){
  var _age;
  function setAge(n){
    _age=n;
  }
  function getAge(){
    return _age;
  }
  return {
    name:name,
    getAge:getAge;
    setAge:setAge;
  }
}
var p1=Person('Bob');
p1.setAge(25)
p1.getAge() // 25
```

函数`Person`的内部变量`_age`，通过闭包`getage`和`setAge`，变成了返回对象`p1`的私有变量

注意，外层函数每次运行，都会生成一个新的闭包，而这个闭包又会保留外层函数的内部变量，所以内存消耗很大。因此不能滥用闭包，否则会造成网页的性能问题。

### 立即调用的函数表达式（IIFE）

`function`关键字出现在行首，一律解释为语句。要解释为表达式，将其放在一个圆括号里面。

```javascript
(function(){})();
```


