# 数组

## 数组的本质

本质上，数组属于一种特殊的对象。`typeof`运算符会返回数组的类型是`object`。
JavaScript 语言规定，对象的键名一律为字符串，所以，数组的键名其实也是字符串。之所以可以用数值读取，是因为非字符串的键名会被转为字符串。
对象有两种读取成员的方法：点结构（`object.key`）和方括号结构（`object[key]`）。但是，对于数值的键名，不能使用点结构。

## in 运算符

检查某个键名是否存在的运算符`in`，适用于对象，也适用于数组。

```javascript
var arr = [ 'a', 'b', 'c' ];
2 in arr  // true
'2' in arr // true
4 in arr // false
```

## for...in 循环和数组的遍历

`for...in`循环不仅可以遍历对象，也可以遍历数组，毕竟数组只是一种特殊对象。

```javascript
let arr=['a','b'];
arr.p='c';
for(let i in arr){
  console.log(i); // 'a','b','c'
}
```

但是，`for...in`不仅会遍历数组所有的数字键，还会遍历非数字键。

## 数组的空位

当数组的某个位置是空元素，即两个逗号之间没有任何值，我们称该数组存在空位（hole）。

```javascript
var a = [1, , 1];
a.length // 3
// 数组的空位是可以读取的，返回`undefined`。
a[1] // undefind
```

### 空位和undefined的区别

如果是空位，使用数组的`forEach`方法、`for...in`结构、以及`Object.keys`方法进行遍历，空位都会被跳过。如果某个位置是`undefined`，遍历的时候就不会被跳过。

## 类似数组的对象

如果一个对象的所有键名都是正整数或零，并且有`length`属性，那么这个对象就很像数组，语法上称为“类似数组的对象”（array-like object）。

```javascript
let obh={
  0:'a',
  1:'b',
  2:'c',
  length:3
}
```

类数组不具有数组特有的方法，典型的“类似数组的对象”是函数的`arguments`对象，以及大多数 DOM 元素集，还有字符串。

```javascript
function args(){return arguments}
var arrayLike=args('a','b')
arrayLike[0] // 'a'
arrayLike.length // 2
arrayLike instanceof Array // false

// DOM元素集
var elts = document.getElementsByTagName('h3');
elts.length // 3
elts instanceof Array // false

// 字符串
'abc'[1] // 'b'
'abc'.length // 3
'abc' instanceof Array // false
```

数组的`slice`方法可以将“类似数组的对象”变成真正的数组。

```javascript
var arr=Array.prototype.slice.call(arrayLike)
```

