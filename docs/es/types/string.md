# 字符串

## 概述

由于 HTML 语言的属性值使用双引号，所以很多项目约定 JavaScript 语言的字符串只使用单引号。

```javascript
// 如果长字符串必须分成多行，可以在每一行的尾部使用反斜杠。
var longStringI = 'Long \
long \
long \
string';
// 也可以使用+号连接
var longStringII = 'Long '
  + 'long '
  + 'long '
  + 'string';
```

## 字符集

JavaScript 使用 Unicode 字符集。JavaScript 引擎内部，所有字符都用 Unicode 表示。每个字符在 JavaScript 内部都是以16位（即2个字节）的 UTF-16 格式储存。JavaScript 的单位字符长度固定为16位长度，即2个字节。
码点`U+1D306`对应的字符为`𝌆，`它写成 UTF-16 就是`0xD834 0xDF06`，4个字节。

```javascript
'𝌆'.length // 2
```

上面代码中，JavaScript 认为`𝌆`的长度为2，而不是1。

总结一下，对于码点在`U+10000`到`U+10FFFF`之间的字符，JavaScript 总是认为它们是两个字符（`length`属性为2）。所以处理的时候，必须把这一点考虑在内，也就是说，JavaScript 返回的字符串长度可能是不正确的。

## Base64 转码

