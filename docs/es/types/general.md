# 数据类型概述

## 简介

JavaScript 的数据类型，共有 7 种。

- 数值（number）：整数和小数（如`1`和`3.14`）
- 字符串（string）：文本（如`Hello World`）
- 布尔值（boolean）：表示真伪，即`true`和`false`
- `undefined`：表示“未定义”或不存在
- `null`：表示空值，即此处的值为空。
- 对象（object）：各种值组成的集合。
- Symbol（es6 引入）：ES5 的对象属性名都是字符串，易造成属性名冲突，因此引入 Symbol，`表示独一无二的值`，如:

```javascript
let s = Symbol();
let a = {[s]:'hello';}
```

数值、字符串、布尔值、Symbol 合称为原始类型（primitive type）的值，是最基本的数据类型。
对象则是合成类型（complex type）的值，一个对象往往是多个原始类型的值的合成。`undefined`和`null`，一般看成两个特殊值。

对象又可分成三个子类型

- 狭义的对象（object）
- 数组（array）
- 函数（function）

## 确定值的类型

- `typeof`运算符 不能区分对象类型 typeof {} typeof []
- `instanceof`运算符 区分实例`__proto__`
- `Object.prototype.toString.call()`方法 区分具体的类型，（不能区分实例）
- `constructor`属性 可以判断实例通过哪个类构造出来的

### typeof

```javascript
typeof 123; // number
typeof '123'; // string
typeof false; // boolean
typeof function f() {}; // function
// 对象返回`object`。
typeof window; // "object"
typeof {}; // "object"
typeof []; // "object"
typeof undefined; // "undefined"
typeof null; // "object"
```

`typeof`可以用来检查一个没有声明的变量，而不报错。

```javascript
// 错误的写法 v未声明
if (v) {
  // ...
}
// ReferenceError: v is not defined

// 正确的写法
if (typeof v === 'undefined') {
  // ...
}
```

`null`的类型是`object`，这是由于历史原因造成的。1995 年的 JavaScript 语言第一版，只设计了五种数据类型（对象、整数、浮点数、字符串和布尔值），没考虑`null`，只把它当作`object`的一种特殊值。后来`null`独立出来，作为一种单独的数据类型，为了兼容以前的代码，`typeof null`返回`object`就没法改变了。

### instanceof

## 函数柯里化

将多个参数变成单一参数，细化函数的功能，让它变得更具体一些

```javascript
// 通用柯里化函数参数
const currying = (fn, arr = []) => {
  // 记录调用时参数的个数 和 函数个数的关系
  let len = fn.length;
  return (...args) => {
    let concatArgs = [...arr, ...args];
    if (concatArgs.length < len) {
      return currying(fn, concatArgs);
    } else {
      return fn(...concatArgs);
    }
  };
};
function isType(typing,content){
    return Object.prototype.toString.call(content) == `[object ${typing}]`
}
let util = {};
['String', 'Number', 'Null', 'Undefined'].forEach((typing) => {
    util['is' + typing] = currying(isType,[typing])
})
console.log(util.isString('hello')) // isType('String','hello')
console.log(util.isNumber('hello')) // isType('Number','hello')
```
