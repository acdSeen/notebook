# 设计模式

## 单例模式

单例模式是`保证一个类只有一个实例，并提供一个访问他的全局访问点。`

```javascript
function Singleton(name) {
  this.name = name;
  this.instance = null;
}

Singleton.prototype.getName = function () {
  console.log(this.name);
};

Singleton.getInstance = function (name) {
  if (!this.instance) {
    this.instance = new Singleton(name);
  }
  return this.instance;
};
// 通过getInstance来获取Singleton的唯一实例
var a = Singleton.getInstance('Bob');
var c = Singleton.getInstance('Aoa');
console.log(a === c); // true
```

## 策略模式

策略模式是`定义一系列的算法，把它们一个个封装起来，并且使它们可以相互替换。`
`相互替换`是相对于静态类型语言的，而 js 则表现为它们具有相同的目标和意图

```javascript
// 定义校验规则，各自封装为策略类，并有相同的目标（校验格式）
var strategies = {
  required: function (val, rule, errMsg) {
    if (rule && val === '') {
      console.log(errMsg ? errMsg : '不能为空');
    }
  },
  max: function (val, rule, errMsg) {
    if (val.length > rule) {
      console.log(errMsg);
    }
  },
  type: function (val, rule, errMsg) {
    if (typeof val !== rule) {
      console.log(errMsg);
    }
  },
};
// 用validateVal函数作为context接受用户的规则
var validateVal = function (rules, val) {
  rules.map((rule) => {
    var ruleKey = Object.keys(rule)[0];
    strategies[ruleKey](val, rule[ruleKey], rule.errMsg);
  });
};
var rules = [
  { required: true },
  { max: 5, errMsg: '最大输入长度 5 个字符' },
  { type: 'number', errMsg: '请输入数字' },
];
validateVal(rules, 'testtttt');
// 最大输入长度 5 个字符
// 请输入数字
```

## 代理模式

代理模式是`为一个对象提供一个代用品或占位符，以边控制对它的访问。`比如说：vue 中要读写 data、methods 里的属性通过代理可以直接使用 this.xxx 进行操作。

```javascript
var topObj = {
  props: {
    name: 'aoa',
    age: 45,
  },
  methods: {
    getData() {
      console.log('获取数据');
    },
  },
};
function proxy(target, sourceKey, key) {
  var sharedDefineProperty = {
    enumerable: true,
    configurable: true,
    get: null,
    set: null,
  };
  sharedDefineProperty.get = function () {
    return target[sourceKey][key];
  };
  sharedDefineProperty.set = function (val) {
    return (target[sourceKey][key] = val);
  };
  Object.defineProperty(target, key, sharedDefineProperty);
}
Object.keys(topObj).map((sourceKey) => {
  Object.keys(topObj[sourceKey]).map((key) => {
    proxy(topObj, sourceKey, key);
  });
});
console.log(topObj.name); // aoa
console.log(topObj.age); // 45
topObj.getData(); // 获取数据
topObj.name = 'Bob';
console.log(topObj.name); // Bob
```

### 虚拟代理

新建一个代理用来合并 http 请求，收集一段时间之内的请求，最后一次性发给服务器。

```javascript
var syncFile = function (id) {
  console.log(`开始同步文件，id为${id}`);
};
// 代理函数proxySynchronousFile 收集一段时间之内的请求
var proxySyncFile = (function () {
  var cache = [], // 保存一段时间内需要同步的id
    timer;
  return function (id) {
    cache.push(id);
    if (timer) {
      // 避免覆盖已经启动的定时器
      return;
    }
    // 2秒后像服务器发送需要同步的id集合
    timer = setTimeout(function () {
      syncFile(cache.join(','));
      clearTimeout(timer);
      timer = null;
      cache = [];
    }, 2000);
  };
})();
var checkboxs = document.getElementsByTagName('input');
for (let i = 0; i < checkboxs.length; i++) {
  checkboxs[i].onclick = function () {
    if (this.checked === true) {
      proxySyncFile(this.id);
    }
  };
}
```

### 缓存代理

缓存之前的计算结果，比如 vue 的计算属性，多次调用时，依赖值不变，则不会重新计算。

```javascript
function mult() {
  var a = 1;
  for (let i = 0; i < arguments.length; i++) {
    a = a * arguments[i];
  }
  return a;
}
var proxyMult = (function () {
  var cache = {};
  return function () {
    var args = Array.prototype.join.call(arguments, ',');
    if (args in cache) {
      // 如果有缓存则直接取缓存，不用计算
      return cache[args];
    }
    return (cache[args] = mult.apply(this, arguments));
  };
})();
console.log(proxyMult(1, 2, 3, 4)); // 24
console.log(proxyMult(1, 2, 3, 4)); // 取缓存 24
```

## 迭代器模式

迭代器模式是指提供一种方法顺序访问一个聚合对象中的各个元素，而又不需要暴露该对象的内部表示。js 有内置实现，比如`Array.prototype.forEach,map,every等`

## 发布-订阅模式

发布—订阅模式又叫观察者模式，它定义对象间的一种一对多的依赖关系，当一个对象的状态发生改变时，所有依赖于它的对象都将得到通知。
如：vue 的响应式原理`数据的变更会触发 DOM 的变化`，eventbus 等。

```javascript
var event = {
  clientList: {},
  listen: function (key, fn) {
    if (!this.clientList[key]) {
      this.clientList[key] = [];
    }
    this.clientList[key].push(fn); // 订阅的消息添加进缓存列表
  },
  trigger: function () {
    var key = Array.prototype.shift.call(arguments),
      fns = this.clientList[key];
    if (!fns || fns.length === 0) {
      // 没有订阅的消息
      return false;
    }
    // 发布消息，所有订阅的都将得到通知
    fns.map((fn) => {
      fn.apply(this, arguments);
    });
  },
  remove: function (key, fn) {
    var fns = this.clientList[key];
    if (!fns) {
      return false; // 无人订阅
    } else if (!fn) {
      //没有指定清空订阅则清空全部订阅
      fns = [];
    } else {
      // 清空指定的订阅
      fns.splice(fns.indexOf(fn), 1);
      console.log(fn + '订阅已清空');
    }
  },
};
// 给对象安装发布订阅功能的函数
function installEvent(obj) {
  for (var i in event) {
    obj[i] = event[i];
  }
}
// 给售楼部对象增加发布订阅功能
var sales = {};
installEvent(sales);
// 小明订阅68平
sales.listen(
  'square68',
  (f1 = function (price) {
    console.log(`68平价格：${price}万`);
  })
);
sales.listen('square68', function (price) {
  console.log(price > 120 ? '买不起' : '掏空6个钱包买');
});
// 小红订阅100平
sales.listen('square100', function (price) {
  console.log(`100平价格：${price}万`);
});
// 发布
sales.trigger('square68', 108); // 68平价格：108万 掏空6个钱包买
sales.trigger('square100', 156); // 100平价格：156万
sales.remove('square68', f1);
// function (price) {
// console.log(`68平价格：${price}万`);
// }订阅已清空
```

## 命令模式

命令模式中的命令（command）指的是一个执行某些特定事情的指令。 用一种松耦合的方式来设计程序，使得请求发送者和请求接收者能够消除彼此之间的耦合关系。`命令模式的由来，其实是回调（callback）函数的一个面向对象的替代品。`

```javascript
var closeDoorCommand = {
  execute: function () {
    console.log('关门');
  },
};

var openPcCommand = {
  execute: function () {
    console.log('开电脑');
  },
};
var MacroCommand = function () {
  return {
    commandsList: [],
    add: function (command) {
      this.commandsList.push(command);
    },
    execute: function () {
      for (var i = 0; this.commandsList.length; i++) {
        this.commandsList[i].execute();
      }
    },
  };
};
var macroCommand = MacroCommand();
macroCommand.add(closeDoorCommand);
macroCommand.add(openPcCommand);
macroCommand.execute();
```

跟许多其他语言不同，JavaScript 可以用高阶函数非常方便地实现命令模式。命令模式在 JavaScript 语言中是一种隐形的模式。

## 组合模式

组合模式就是用小的子对象来构建更大的对象，而这些小的子对象本身也许是由更小的“孙对象”构成的。

文件夹里既可以包含文件，又可以包含其他文件夹，最终可能组合成一棵树。

```javascript
function Folder(name) {
  this.name = name;
  this.files = [];
}
Folder.prototype.add = function (file) {
  this.files.push(file);
};
Folder.prototype.scan = function () {
  console.log('开始扫描文件夹：' + this.name);
  for (let i = 0; i < files.length; i++) {
    files[i].scan();
  }
};
function File(name) {
  this.name = name;
}
File.prototype.scan = function () {
  console.log('开始扫描文件：' + this.name);
};
var folder = new Folder('学习资料');
var folder1 = new Folder('JavaScript');
var folder2 = new Folder('jQuery');

var file1 = new File('JavaScript设计模式与开发实践');
var file2 = new File('精通jQuery');
var file3 = new File('重构与模式');

folder1.add(file1);
folder2.add(file2);

folder.add(folder1);
folder.add(folder2);
folder.add(file3);

var folder3 = new Folder('Nodejs');
var file4 = new File('深入浅出Node.js');
folder3.add(file4);
var file5 = new File('JavaScript语言精髓与编程实践');
folder.add(folder3);
folder.add(file5);
folder.scan(); // 扫码整个文件夹。只需要操作树顶端
/**
 *                                           学习资料
 *                                               |
 *              ----------------------------------------------------------------------------
 *              |                       |          |             |                         |
 *         JavaScript                jQuery    重构与模式      Nodejs        JavaScript语言精髓与编程实践
 *              |                       |                        |
 * JavaScript设计模式与开发实践    精通jQuery               深入浅出Node.js
 */
```

## 模板方法模式

模板方法模式由两部分结构组成，第一部分是抽象父类，第二部分是具体的实现子类。通常`在抽象父类中封装了子类的算法框架，包括实现一些公共方法以及封装子类中所有方法的执行顺序。`子类通过继承这个抽象类，也继承了整个算法结构，并且可以选择重写父类的方法。

### 钩子方法

模拟咖啡和茶的冲泡顺序，共同点：1.烧开水 2.倒开水冲泡 3.把饮料倒进杯子 4.加调料。但有个别客人不需要加调料，这样就少了一个步骤。`钩子方法（hook）`解决这个问题，放置钩子是隔离变化的一种常见手段。

```javascript
// 定义饮料类（父类）
function Beverage() {}
Beverage.prototype.boilWater = function () {
  console.log('烧开水');
};
Beverage.prototype.brew = function () {
  throw new Error('子类必须重写brew方法');
};
Beverage.prototype.pourInCup = function () {
  throw new Error('子类必须重写 pourInCup 方法');
};
Beverage.prototype.addCondiments = function () {
  throw new Error('子类必须重写 addCondiments方法');
};
Beverage.prototype.customerWantsCondiments = function () {
  return true; // 默认需要调料
};
Beverage.prototype.init = function () {
  // 封装子类的算法框架及执行顺序
  this.boilWater();
  this.brew();
  this.pourInCup();
  if (this.customerWantsCondiments()) {
    // 如果挂钩返回 true，则需要调料
    this.addCondiments();
  }
};
// 定义咖啡类并使用寄生组合式继承
function Coffer() {
  Beverage.call(this); // 借用构造函数继承
}
// 寄生式继承
function inherit() {
  let prototype = Object.create(Beverage.prototype); // 原型式继承
  prototype.constructor = Coffer;
  Coffer.prototype = prototype;
}
inherit();
Coffer.prototype.brew = function () {
  console.log('用沸水冲泡咖啡');
};

Coffer.prototype.pourInCup = function () {
  console.log('把咖啡倒进杯子');
};

Coffer.prototype.addCondiments = function () {
  console.log('加糖和牛奶');
};
Coffer.prototype.customerWantsCondiments = function () {
  return window.confirm('请问需要调料吗？');
};
var newCoffer = new Coffer();
newCoffer.init();
// VM162:4 烧开水
// VM162:38 用沸水冲泡咖啡
// VM162:42 把咖啡倒进杯子
// VM162:46 加糖和牛奶
```

## 享元模式

享元（flyweight）是一种用于性能优化的模式，核心是运用共享技术支持大量细粒度的对象。享元模式要求将对象的属性划分为内部状态和外部状态。

- 内部状态存储于对象内部。
- 内部状态可以被一些对象共享
- 内部状态独立于具体的场景，通常不会改变。
- 外部状态取决于具体的场景，并根据场景而变化，外部状态不能被共享。

```javascript
function Model(sex) {
  this.sex = sex;
}
Model.prototype.takePhoto = function () {
  // this.sex内部状态 this.underwear外部状态
  console.log(this.sex + this.underwear);
};
var maleModel = new Model('male'),
  femaleModel = new Model('female');
for (var i = 1; i <= 50; i++) {
  // 外部状态根据场景而变化
  maleModel.underwear = 'underwear' + i;
  maleModel.takePhoto();
}
for (var j = 1; j <= 50; j++) {
  femaleModel.underwear = 'underwear' + j;
  femaleModel.takePhoto();
}
```

## 职责链模式

职责链模式的定义是：使多个对象都有机会处理请求，从而避免请求的发送者和接收者之间的耦合关系，将这些对象连成一条链，并沿着这条链传递该请求，直到有一个对象处理它为止。

```javascript
var order500 = function (orderType, pay, stock) {
  if (orderType === 1 && pay === true) {
    console.log('500元定金预购，得到 100优惠券');
  } else {
    return 'nextSuccessor'; // 我不知道下一个节点是谁，反正把请求往后面传递
  }
};
var order200 = function (orderType, pay, stock) {
  if (orderType === 2 && pay === true) {
    console.log('200元定金预购，得到 50 优惠券');
  } else {
    return 'nextSuccessor'; // 我不知道下一个节点是谁，反正把请求往后面传递
  }
};
var orderNormal = function (orderType, pay, stock) {
  if (stock > 0) {
    console.log('普通购买，无优惠券');
  } else {
    console.log('手机库存不足');
  }
};
// Chain.prototype.setNextSuccessor  指定在链中的下一个节点
// Chain.prototype.passRequest  传递请求给某个节点
function Chain(fn) {
  this.fn = fn;
  this.successor = null;
}
Chain.prototype.setNextSuccessor = function (successor) {
  return (this.successor = successor);
};
Chain.prototype.passRequest = function () {
  var ret = this.fn.apply(this, arguments);
  if (ret === 'nextSuccessor') {
    return (
      this.successor &&
      this.successor.passRequest.apply(this.successor, arguments)
    );
  }
  return ret;
};
// 现在我们把 3个订单函数分别包装成职责链的节点：
var chainOrder500 = new Chain(order500);
var chainOrder200 = new Chain(order200);
var chainOrderNormal = new Chain(orderNormal);
// 然后指定节点在职责链中的顺序：
chainOrder500.setNextSuccessor(chainOrder200);
chainOrder200.setNextSuccessor(chainOrderNormal);
// 最后把请求传递给第一个节点：
chainOrder500.passRequest(1, true, 500); // 输出：500 元定金预购，得到 100优惠券
chainOrder500.passRequest(2, true, 500); // 输出：200 元定金预购，得到 50优惠券
chainOrder500.passRequest(3, true, 500); // 输出：普通购买，无优惠券
chainOrder500.passRequest(1, false, 0); // 输出：手机库存不足
// 通过改进，我们可以自由灵活地增加、移除和修改链中的节点顺序，假如某天网站运营人员想出了支持 300元定金购买，那我们就在该链中增加一个节点即可：
var order300 = function () {
  // 具体实现略
};
chainOrder300 = new Chain(order300);
chainOrder500.setNextSuccessor(chainOrder300);
chainOrder300.setNextSuccessor(chainOrder200);
```

无论是作用域链、原型链，还是 DOM节点中的事件冒泡，我们都能从中找到职责链模式的影子。

## 中介者模式

中介者模式的作用就是解除对象与对象之间的紧耦合关系。增加一个中介者对象后，所有的相关对象都通过中介者对象来通信，而不是互相引用，所以当一个对象发生改变时，只需要通知中介者对象即可。中介者使各对象之间耦合松散，而且可以独立地改变它们之间的交互。

## 装饰者模式

给对象动态的增加职责的方式称为装饰者（decorator）模式。

## 状态模式

状态模式的关键是区分事物内部的状态，事物内部状态的改变往往会带来事物的行为改变。

## 适配器模式

适配器的别名是包装器（wrapper），当我们试图调用模块或者对象的某个接口时，却发现这个接口的格式并不符合目前的需求。这时候有两种解决办法，第一种是修改原来的接口实现，但如果原来的模块很复杂，或者我们拿到的模块是一段别人编写的经过压缩的代码，修改原接口就显得不太现实了。第二种办法是创建一个适配器，将原接口转换为客户希望的另一个接口，客户只需要和适配器打交道。

## 设计原则和编程技巧

设计原则通常指的是`单一职责原则、里氏替换原则、依赖倒置原则、接口隔离原则、合成复用原则和最少知识原则`。


### 单一职责原则

单一职责（SRP）的职责被定义为“引起变化的原因”。SRP原则体现为：一个对象（方法）只做一件事情。

### 最少知识原则

最少知识原则（LKP）说的是一个软件实体应当尽可能少地与其他实体发生相互作用。
外观模式的主要特点有两点：
- 为一组子系统提供一个简单便利的访问入口
- 隔离客户与复杂子系统之间的联系，客户不用去了解子系统的细节

### 开放封闭原则

开放-封闭原则（OCP）的定义是：
- 软件实体（类、模块、函数）等应该是可以扩展的，但是不可修改。

