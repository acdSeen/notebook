# TypeScript

## 一、TypeScript 与 JavaScript 的区别

| TypeScript                                     | JavaScript                                 |
| ---------------------------------------------- | ------------------------------------------ |
| JavaScript 的超集用于解决大型项目的代码复杂性  | 一种脚本语言，用于创建动态网页             |
| 可以在编译期间发现并纠正错误                   | 作为一种解释型语言，只能在运行时发现错误   |
| 强类型，支持静态和动态类型                     | 弱类型，没有静态类型选项                   |
| 最终被编译成 JavaScript 代码，使浏览器可以理解 | 可以直接在浏览器中使用                     |
| 支持模块、泛型和接口                           | 不支持模块，泛型和接口                     |
| 社区的支持仍在增长，而且还不是很大             | 大量的社区支持以及大量文档和解决问题的支持 |

## 二、基础类型

```typescript
// 一、Boolean类型
let isDone: boolean = false;

// 二、Number类型
let count: number = 10;

// 三、String类型
let name: string = 'semlie';

// 四、Symbol类型
const aym = Symbol();
let obj = {
  [aym]: 'semlie',
};

// 五、Array类型
let list: number[] = [1, 2, 3];
let arr: Array<string> = ['a', 'b', 'c'];

// 六、Enum类型
// 使用枚举类型可以定义一些带妹子的常量。使用枚举可以清晰的表达意图或创建一组有区别的用例。Typescript支持数字的和基于字符串的枚举。
// 1. 数字的枚举
enum Direction {
  NORTH = 3, // 默认为0，可设置为3
  SOUTH,
  EAST,
  WEST,
}
let dir: Direction = Direction.NORTH; // 3
let air: Direction = Direction.SOUTH; // 4
// 支持反向映射
let dirName = Direction[0]; // NORTH
// 2. 字符串枚举
enum StrDirection {
  NORTH = 'NORTH',
  SOUTH = 'SOUTH',
  EAST = 'EAST',
  WEST = 'WEST',
}
let dir: StrDirection = Direction.NORTH; // 'NORTH'
let air: StrDirection = Direction.SOUTH; // 'SOUTH'
// 3. 常量枚举
// 使用const修饰额枚举，使用内联语法，不会为枚举类型编译生成任何javascript
const enum ConDirection {
  NORTH,
  SOUTH,
  EAST,
  WEST,
}
let condir: ConDirection = ConDirection.NORTH;
// 编译为es5代码如下，没有枚举类型的js
var condir = 0; /* NORTH */
// 4. 异构枚举
// 异构枚举的成员值是数字和字符串的混合
enum Enum {
  A,
  B,
  c = 'C',
  d = 'D',
  e = 8,
}
// 生成的 ES5 代码，数字枚举相对字符串枚举多了 “反向映射”：
var Enum;
(function (Enum) {
  Enum[(Enum['A'] = 0)] = 'A';
  Enum[(Enum['B'] = 1)] = 'B';
  Enum['C'] = 'C';
  Enum['D'] = 'D';
  Enum[(Enum['E'] = 8)] = 'E';
  Enum[(Enum['F'] = 9)] = 'F';
})(Enum || (Enum = {}));

// 七、Any类型
// 任何类型都可以被归为any类型，是顶级类型也称为全局超级类型
let dynamic: any = 123;
dynamic = 'baba';
dynamic = true;
// any类型本质上是类型系统的一个逃逸舱，TypeScript运行对any类型的值执行任何操作，这很容易变形类型正确但在运行时有问题的代码，所以引入unknown类型。

// 八、Unknown类型
// unknown是ts类型系统的另一种顶级类型，
let val: unknown;
let val1: unknown = val; //ok
let val2: any = val; //ok
let val3: string = val; //error
// unknown类型只能被赋值给any和unknown类型本身，

// 九、tuple类型
// 数组异步由同种类型的值组成，但有时需要在单个变量中存不同类型的值，这时可以使用元组
let tupleType: [string, boolean];
tupleType = ['semlinker', true];

// 十、Void类型
// void类型和any类型相反，表示没有任何类型。
function warnUser(): void {
  console.log('this is my warning msg');
}

// 十一、null和undefined类型

// 十二、object、Object、{}类型
// 1.object是ts2.2引入的新类型，表示非原始类型
interface ObjectConstructor {
  create(o: object | null): any;
}
const proto = {};
Object.create(proto); //ok
Object.create(null); //ok
Object.create(13222); //error
// 2.Object类型：是所有Object类的实例的类型，由两个接口来定义
// Object接口定义了Object.prototype原型对象上的属性
interface Object {
  constructor: Function;
  toString(): string;
  toLocaleString(): string;
  valueOf(): Object;
  hasOwnProperty(v: PropertyKey): boolean;
  isPrototypeOf(v: Object): boolean;
  propertyIsEnumerable(v: PropertyKey): boolean;
}
// ObjectConstructor接口定义了Object类的属性。
interface ObjectConstructor {
  new (value?: any): Object;
  (value?: any): any;
  readonly prototype: Object;
  getPrototypeOf(o: any): any;
}
declare var Object: ObjectConstructor;
// {}类型描述了一个没有成员的对象，
const obj = {};

// 十三、Never类型
// never表示永远不存在的值的类型，如never是那些总会抛出异常或不会有返回值的函数的返回值的类型
function error(msg: string): never {
  throw new Error(msg);
}
function infiniteLoop(): never {
  while (true) {}
}
// 利用never特性进行全面性检查
type Foo = string | number;
function control(foo: Foo) {
  if (typeof foo === 'string') {
  } else {
    const check: never = foo;
  }
}
// 使用 never 避免出现新增了联合类型没有对应的实现，目的就是写出类型绝对安全的代码。
```

## 三、TypeScript 断言

### 3.1 类型断言

```typescript
let someValue: any = 'this is a string';
// 1.尖括号语法
let strLength: number = (<string>someValue).length;
// 2.as语法
let strLength: number = (someValue as string).length;
```

### 3.2 非空断言

当上下文类型检测器无法断定类型时，后缀表达式操作符！可以用于断言操作对象是非 null 和非 undefined 类型。**_具体而言，x!将从 x 值域中排除 null 和 undefined_**。

```typescript
// 1.忽略undefined和null类型
function myFunc(maybeString: string | undefined | null) {
  const onlyString: string = maybeString; // error
  const ignoreUndefinedAndNull: string = maybeString!; // ok
}
// 2.调用函数时忽略undefined类型
type NumGenerator = () => number;
function myFunc(numGenerator: NumGenerator | undefined) {
  const num1 = numGenerator(); // error
  const num2 = numGenerator!(); // ok
}
```

### 3.3 确定赋值断言

即运行在实例属性和变量声明后面放置一个**!**号，从而告诉 TypeScript 该属性会被明确的赋值。

```typescript
let x!: number; // 确定赋值断言
initialize();
console.log(2 * x);
function initialize() {
  x = 10;
}
```

## 四、类型守卫

**类型保护是可执行运行时检查的一种表达式，用于确保该类型在一定的范围内。**类型保护可以保证一个字符串是一个字符串，尽管它的值可以是一个数值。

```typescript
// 4.1 in关键字
interface Admin {
  name: string;
  privileges: string[];
}
interface Employee {
  name: string;
  startDate: Date;
}
type UnknownEmployee = Employee | Admin;
function printEmployeeInformation(emp: UnknownEmployee) {
  console.log('Name:' + emp.name);
  if ('privileges' in emp) {
    console.log('Privileges:' + emp.privileges);
  }
  if ('startDate' in emp) {
    console.log('Start Date:' + emp.startDate);
  }
}
// 4.2 typeof关键字
function padLeft(value: string, padding: string | number) {
  if (typeof padding === 'number') {
    return Array(padding + 1).join('') + value;
  }
  if (typeof padding === 'string') {
    return padding + value;
  }
  throw new Error(`Expected string or number, got '${padding}'.`);
}
// 4.3 instanceof关键字
interface Padder {
  getPaddingString(): string;
}
class SpaceRepeatingPadder implements Padder {
  constructor(private numSpaces: number) {}
  getPaddingString() {
    return Array(this.numSpaces + 1).join('');
  }
}
class StringPadder implements Padder {
  constructor(private value: string) {}
  getPaddingString() {
    return this.value;
  }
}
let padder: Padder = new SpaaceRepeatingPadder(6);
if (padder instanceof SpaceRepeatingPadder) {
  // padder的类型收窄为 'SpaceRepeatingPadder'
}
// 4.4 自定义类型保护的类型谓词
function isNumber(x: any): x is number {
  return typeof x === 'number';
}
function isString(x: any): x is string {
  return typeof x === 'string';
}
```

## 五、联合类型和类型别名

### 5.1 联合类型

联合类型通程与**null**或**undefined**一起使用

```typescript
const sayHello = (name: string | undefined) => {};
sayHello('semlinker');
sayHello(undefined);
```

### 5.2 可辨识联合

TypeScript 可辨识联合（Discriminated Unions）类型，也称为代数数据类型或标签联合类型，**可辨识、联合类型和类型守卫**。
**如果一个类型是多个类型的联合类型，且多个类型含有一个公共属性，那么就可以利用这个公共属性，来创建不同的类型保护区块。**

```typescript
// 1.可辨识
// 可辨识要求联合类型中的每个元素都含有一个单例类型属性
enum CarTransmission {
  Automatic = 200,
  Manual = 300,
}
interface Motorcycle {
  vType: 'motorcycle';
  make: number;
}
interface Car {
  vType: 'car';
  transmission: CarTransmission;
}
interface Truck {
  vType: 'truck';
  capacity: number;
}
// Motorcycle、Car、Truck三个接口，都包含一个vType属性，该属性被称为可辨识的属性，而其他属性只跟特性的接口相关。
// 2.联合类型
type Vehicle = Motorcycle | Car | Truck;
// 3.类型守卫
const EVALUATION_FACTOR = Math.PI;
function evaluatePrice(vehicle: Vehicle) {
  return vehicle.capacity * EVALUATION_FACTOR;
}
const myTruck: Truck = { vType: 'truck', capacity: 9.5 };
evaluatePrice(myTruck);
// 上述代码会报错
// Property 'capacity' does not exist on type 'Vehicle'.
// Property 'capacity' does not exist on type 'Motorcycle'.
// 在Motorcycle、Car接口中不存在capacity属性，这时使用 switch case 实现类型守卫。
function evaluatePrice(vehicle: Vehicle) {
  switch (vehicle.vType) {
    case 'car':
      return vehicle.transmission * EVALUATION_FACTOR;
    case 'truck':
      return vehicle.capacity * EVALUATION_FACTOR;
    case 'motorcycle':
      return vehicle.make * EVALUATION_FACTOR;
  }
}
```

### 5.3 类型别名

给一个类型起新名字

```typescript
type Message = string | string[];
let greet = (message: Message) => {};
```

## 六、交叉类型

## 七、TypeScript 函数

### 7.1 typescript 函数与 javascript 函数的区别

| TypeScript     | JavaScript         |
| -------------- | ------------------ |
| 含有类型       | 无类型             |
| 箭头函数       | 箭头函数（ES2015） |
| 函数类型       | 无函数类型         |
| 必填和可选参数 | 所有参数都是可选的 |
| 默认参数       | 默认参数           |
| 剩余参数       | 剩余参数           |
| 函数重载       | 无函数重载         |

## 八、TypeScript 数组

## 九、TypeScript 对象

## 十、TypeScript 接口

### 10.1 对象的形状

```typescript
interface Person {
  name: string;
  age: number;
}
let semlinker: Person = {
  name: 'some',
  age: 18,
};
```

### 10.2 可选|只读属性

```typescript
interface Person {
  readonly name: string;
  age?: number;
}
```

### 10.3 任意属性

使用**索引签名**的形式来满足任意属性

```typescript
interface Person {
  name: string;
  age?: number;
  [propName: string]: any;
}
```

### 10.4 接口与类型别名的区别

#### 1.Objects/Functions

```typescript
// 接口和类型别名都可以用来描述对象的形状或函数签名
// 接口
interface Point {
  x: number;
  y: number;
}
interface SetPoint {
  (x: number, y: number): void;
}
// 类型别名
type Point = {
  x: number;
  y: number;
};
type SetPoint = (x: number, y: number) => void;
```

#### 2.Other Types

与接口类型不一样，类型别名可以用于一些其他类型，比如原始类型，联合类型和元组；

```typescript
// primitive
type Name = string;
// object
type PartialPointX = { x: number };
type PartialPointY = { y: number };
// union
type PartialPoint = PartialPointX | PartialPointY;
// tuple
type Data = [number, string];
```

#### 3.Extend

接口和类型别名都能被扩展，但语法有所不同。接口可以扩展类型别名，而反过来是不行的

```typescript
// interface extends interface
interface PartialPointX {
  x: number;
}
interface Point extends PartialPointX {
  y: number;
}
// type alias extends type alias
type PartialPointX = { x: number };
type Point = PartialPointX & { y: number };
// interface extends type alias
type PartialPointX = { x: number };
interface Point extends PartialPointX {
  y: number;
}
// type alias extends interface
interface PartialPointX {
  x: number;
}
type Point = PartialPointX & { y: number };
```

#### 4.Implements

类可以以相同的方式实现接口或类型别名，但类不能实现使用类型别名定义的联合类型：

```typescript
interface Point {
  x: number;
  y: number;
}
class SomePoint implements Point {
  x = 1;
  y = 2;
}
type Point2 = {
  x: number;
  y: number;
};
class SomePoint2 implements Point2 {
  x = 1;
  y = 2;
}
type PartialPoint = { x: number } | { y: number };
// error
// A class can only implement an object type intersection of object types with statically known members.
class SomePartialPoint implements PartialPoint {
  x = 1;
  y = 2;
}
```

#### 5.Declaration merging

接口可以定义多次，会被自动合并为单个接口。

```typescript
interface Point {
  x: number;
}
interface Point {
  y: number;
}
const point: Point = { x: 1, y: 2 };
```

## 十一、TypeScript 类

### 11.1 类的属性和方法

```typescript
class Greeter {
  // 静态属性
  static cname: string = 'Greeter';
  // 成员属性
  greeting: string;
  // 构造函数-执行初始化操作
  constructor(message: string) {
    this.greeting = message;
  }
  // 静态方法
  static getClassName() {
    return 'Class name is Greeter';
  }
  // 成员方法
  greet() {
    return 'hello,' + this.greeting;
  }
}
let greeter = new Greeter('world');
```

### 11.2 ECMAScript 私有字段

```typescript
class Person {
  #name: string;
  constructor(name: string) {
    this.#name = name;
  }
}
let semlinker = new Person('Semlinker');
semlinker.#name; // Property '#name' is not accessible outside class 'Person'
/**
 * 私有字段以#字符开头，有时我们称之为私有名称；
 * 每个私有字段名称都唯一的限定于其包含的类；
 * 不能再私有字段上使用typescript可访问性修饰符 如：public...
 * 私有字段不能再包含的类之外访问，不能被检测到。
 * **/
```

### 11.3 访问器

在 typescript 中，可以通过**getter**和**setter**方法来实现数据的封装和有效性校验，防止出现异常数据。

```typescript
let passcode = 'Hello TypeScript';
class Employee {
  private __fullName: string;
  get fullName(): string {
    return this._fullName;
  }
  set fullName(newName: string) {
    if (passcode && passcode == 'Hello TypeScript') {
      this._fullName = newName;
    } else {
      console.log('error:Unauthorized update of employee!');
    }
  }
}
let employee = new Employee();
employee.fullName = 'Semlinker';
if (employee.fullName) {
  console.log(employee.fullName);
}
```

### 11.4 类的继承

### 11.5 抽象类

抽象类不能被实例化，因为它里面包含一个或多个抽象方法，所谓抽象方法，是指不包含具体实现的方法；

```typescript
abstract class Person {
  constructor(public name: string) {}
  abstract say(words: string): void;
}
```

### 11.6 类方法重载

## 十二、TypeScript 泛型

**在 C#和 Java 这样的语言中，可以使用泛型来创建可重用的组件，一个组件可以支持多种类型的数据。这样用户就可以以自己的数据类型来使用组件。**

- T(Type): T 可以用任何有效名称代替。
- V(Value): 表示对象中的值类型。
- K(Key): 表示对象中的键类型。
- E(Element): 表示元素类型。

```typescript
function identity<T, U>(value: T, message: U): T {
  console.log(message);
  return value;
}
// 为类型变量显示设定值
console.log(identity<Number, string>(68, 'Semlinker'));
console.log(identity(68, 'Semlinker'));
// 泛型接口
interface GenericIdentityFn<T> {
  (arg: T): T;
}
// 泛型类
class GenericNumber<T> {
  zeroValue: T;
  add: (x: T, y: T) => T;
}
let myGenericNumber = new GenericNumber<number>();
myGenericNumber.zeroValue = 0;
myGenericNumber.add = function (x, y) {
  return x + y;
};
```

### 泛型工具类型

```typescript
// 1.typeof 用来获取一个变量声明或对象的类型
interface Person {
  name: string;
  age: number;
}
const sem: Person = { name: 'semlinker', age: 33 };
type Sem = typeof sem; // Person
function toAarray(x: number): number[] {
  return [x];
}
type Func = typeof toArray; // (x:number)=>number[]
// 2.keyof 用于获取某种类型的所有键，其返回类型是联合类型
interface Person {
  name: string;
  age: number;
}

type K1 = Person; // 'name' | 'age'
type K2 = keyof Person[]; // 'length' | 'toString' | 'pop' | 'push' | 'concat' | 'join'
type K3 = keyof { [x: string]: Person }; // string | number
// typescript支持两种索引签名，数字索引和字符串索引：
interface StringArray {
  // 字符串索引 -> keyof StringArray => string | numberk
  [index: string]: string;
}
interface StringArrayl {
  // 数字索引 -> keyof StringArrayl => number
  [index: number]: string;
}
// js在执行索引操作时会先把数值索引转换为字符串索引，所以keyof {[x:string]:Peerson} 返回string | number
// 3.in 用来遍历枚举类型
type Keys = 'a' | 'b' | 'c';
type Obj = {
  [p in Keys]: any;
}; // -> {a:any,b:any,c:any}
// 4.infer 在条件类型语句中，可以用infer声明一个类型变量并且对它进行使用。
type ReturnType<T> = T extends (...args: any[]) => infer R ? R : any;
// infer R 就是声明一个变量来承载传入函数签名的返回值类型，就是用它取到函数返回值的类型方便之后使用
// 5.extends 定义的泛型不想过于灵活或者说想继承某些类等，可以通过extends关键字添加泛型约束。
interface Lengthwise {
  length: number;
}
function loggingIdentity<T extends Lengthwise>(arg: T): T {
  console.log(arg.length);
  return arg;
}
// 6.partial Partial<T>的作用是将某个类型里的属性全部变成可选项？
interface Todo {
  title: string;
  description: string;
}
function uupdateTodo(todo: Todo, fieldsToUpdate: Partial<Todo>) {
  return { ...todo, ...fieldsToUpdate };
}
```

## 十三、TypeScript 装饰器

- 类装饰器（class decorators）
- 属性装饰器（property decorators）
- 方法装饰器（method decorators）
- 参数装饰器（parameter decorators）

```typescript
function Greeter(greeting: string) {
  return function (target: Function) {
    target.prototype.greet = function (): void {
      console.log(greeting);
    };
  };
}
// 类装饰器
@Greeter('hello ts!')
class Greeting {
  constructor() {}
}
let myGreeting = new Greeting();
(myGreeting as any).greet();
// 属性装饰器
function logProperty(target: any, key: string) {
  delete target[key];
  const backingField = '_' + key;
  Object.defineProperty(target, backingField, {
    writable: true,
    enumerable: true,
    configurable: true,
  });
  // property getter
  const getter = function (this: any) {
    const currVal = this[backingField];
    console.log(`Get:${key}=>${currVal}`);
    return currVal;
  };
  // property setter
  const setter = function (this: any, newVal: any) {
    console.log(`Set:${key}=>${newVal}`);
    this[backingField] = newVal;
  };
  Object.defineProperty(target, key, {
    get: getter,
    set: setter,
    enumerable: true,
    configurable: true,
  });
}
class Person {
  @logProperty
  public name: string;
  constructor(name: string) {
    this.name = name;
  }
}
const pl = new Person('semlinker');
pl.name = 'kakuqo';
```

## 十四、TypeScript4.0 新特性

## 十五、编译上下文

### 15.1 tsconfig.json 的作用

- 用于标识 typescript 项目的根路径；
- 用于配置 typescript 编译器；
- 用于指定编译的文件。

### 15.2 tsconfig.json 主要字段

- files 设置要编译文件的名称
- include 设置需要进行编译的文件，支持路径模式匹配；
- exclude 设置无需进行编译的文件，支持路径模式匹配；
- compilerOptions 设置与编译流程相关的选项；

### 15.3 compilerOptions 选项
