class GenericCreator<T> {
  create<T>(c: { new(a: number): T }, num: number): T {
    return new c(num);
  }
}
class Person {
  age;
  constructor(age: number) {
    this.age = age;
  }
}
const creatorl = new GenericCreator<Person>();
const XiaoM: Person = creatorl.create(Person, 18);
console.log(XiaoM.age)