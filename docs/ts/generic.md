# 泛型

## 泛型接口

```typescript
interface Identities<V, M> {
  value: V;
  message: M;
}
function identity<T, U>(value: T, message: U): Identities<T, U> {
  console.log(value + ':' + typeof value);
  console.log(message + ':' + typeof message);
  let identities: Identities<T, U> = {
    value,
    message,
  };
  return identities;
}
console.log(identity(68, 'Semlinker'));
```

## 泛型类

```typescript
interface GenericInterface<U> {
  value: U;
  geteIdentity: () => U;
}
class IdentityClass<T> implements GenericInterface<T> {
  value: T;
  constructor(value: T) {
    this.value = value;
  }
  getIdentity(): T {
    return this.value;
  }
}
const myNumberClass = new IdentityClass<Number>(68);
console.log(myNumberClass.getIdentity()); // 68
```

## 泛型约束

```typescript
function identity<T>(arg: T): T {
  console.log(arg.length); // error
  return arg;
}
// 让类型变量extends一个含有所需属性的接口
interface Length {
  length: number;
}
function identity<T extends Length>(arg: T): T {
  console.log(arg.length); // 可以获取length属性
  return arg;
}
// 或者显式的将变量设置为数组类型
function identity<T>(arg: T[]): T[] {
  console.log(aarg.length);
  return arg;
}
// 限制输入的属性名包含在keyof返回的联合类型中
// K extends keyof T 确保key一定是obj中包含的键
function getProperty<T, K extends keyof T>(obj: T, key: K): T[K] {
  return obj[key];
}
enum Difficulty {
  Easy,
  Intermediate,
  Hard,
}
let tsInfo = {
  name: 'Typescript',
  supersetOf: 'Javascript',
  difficulty: Difficulty.Intermedialte,
};
let difficulty: Difficulty = getProperty(tsInfo, 'difficulty');
```

## 泛型参数默认类型

当使用泛型时没有在代码中直接指定类型参数，从实际值参数中也无法推断出类型时，这个默认类型就会起作用。

```typescript
interface A<T = string> {
  name: T;
}
const strA: A = { name: 'semlinker' };
const numB: A<number> = { name: 101 };
```

## 泛型条件类型

```typescript
interface Dictionary<T = any> {
  [key: string]: T;
}
type StrDict = Dictionary<string>;
type DictMember<T> = T extends Dictionary<infer V> ? V : never;
type StrDIctMember = DictMember<StrDict>; //string
// StrDict是Dictionary<string> 满足 T extends Dictionary 使用infer声明一个变量V，并反回V类型。就是string
// 利用条件类型和infer关键字，获取promise对象的返回值类型
async function stringPromise() {
  return 'hello,semlinker';
}
interface Person {
  name: string;
  age: string;
}
async function personPromise() {
  return { name: 'Semlinker', age: 30 } as Person;
}
type PromiseType<T> = (args: any[]) => Promise<T>;
type UnPromisify<T> = T extends PromiseType<infer U> ? U : never;

type extractStringPromise = UnPromisify<typeof stringPromise>; // string
type extractPersonPromise = UnPromisify<typeof personPromise>; // Person
// record 用作将K中所有的属性的值转化为T类型
type Record<K extends keyof any, T> = {
  [P in K]: T;
};
interface PageInfo {
  title: string;
}
type Page = 'home' | 'about' | 'contact';
const x: Record<Page, PageInfo> = {
  about: { title: 'about' },
  contact: { title: 'contact' },
  home: { title: 'home' },
};
// pick 将某个类型中的子属性挑出来，变成包含这个类型部分属性的子类型。
interface Todo {
  title: string;
  description: string;
  completed: boolean;
}
type TodoPreview = Pick<Todo, 'title' | 'complete'>;
const todo: TodoPreview = {
  title: 'clean room',
  completed: false,
};
// exclude 将某个类型中属于另一个的类型移除掉。
type T0 = Exclude<'a' | 'b' | 'c', 'a'>; // 'b'|'c'
type T1 = Exclude<string | number | (() => void), Function>; // string | number
// ReturnType 用于获取函数T的返回类型
type T2 = ReturnType<() => string>; //string
type T3 = ReturnType<<T extends U, U extends number[]>() => T>; // number[]
```

## 使用泛型创建对象

```typescript
class FirstClass {
  id: number | undefined;
}
class SecondClass {
  name: string | undefined;
}
class GenericCreator<T> {
  create(): T {
    // error   T only refers to a type,but is being used as a value here.
    return new T();
  }
}
const creatorl = new GenericCreator<FirstClass>();
const firstClass: FirstClass = creatorl.create();
const creator2 = new GenericCreator<SecondClass>();
const secondClass: SecondClass = creator2.create();

// 使用new关键字来描述一个构造对象
interface Point {
  new (x: number, y: number): Point;
}
// new (x:number,y:number)称之为构造签名，
```

### 构造函数类型

- 包含一个或多个构造签名的对象类型被称为构造函数类型；
- 构造函数类型可以使用构造函数类型字面量或包含构造签名的对象类型字面量来编写。

```typescript
// 把接口的属性和构造函数类型进行分类
interface Point {
  x: number;
  y: number;
}
interface PointConstructor {
  new (x: number, y: number): Point;
}
class Point2D implements Point {
  readonly x: number;
  readonly y: number;
  constructor(x: number, y: number) {
    this.x = x;
    this.y = y;
  }
}
function newPoint(
  pointConstructor: PointConstructor,
  x: number,
  y: number
): Point {
  return new pointConstructor(x, y);
}
const point: Point = newPoint(Point2D, 1, 2);
```

### 使用泛型创建对象

```typescript
class GenericCreator<T> {
  create<T>(c: { new(a: number): T }, num: number): T {
    return new c(num);
  }
}
class Person {
  age;
  constructor(age: number) {
    this.age = age;
  }
}
const creatorl = new GenericCreator<Person>();
const XiaoM: Person = creatorl.create(Person, 18);
console.log(XiaoM.age) // 18
```

