# TS 中的符号

## !非空断言

```typescript
// 忽略undefined和null类型
function fn(maybeString: string | null | undefined) {
  const isString: string = mayneString!;
}
// 调用函数时忽略undefined类型
type NumGenerator = () => number;
function myFunc(numGenerator: NumGenerator | undefined) {
  const num1 = numGenerator!();
}
// 确定赋值断言
let x!: number;
initialize();
console.log(2 * x); // let x!:number 确定赋值断言
function initialize() {
  x = 10;
}
```

## ?.运算符

ts3.7 实现的可选链（Optional Chaining），如果遇到**null**和**undefined**就可以立即停止某些表达式的运行。

```typescript
if (a && a.b) {
}
// 用可选链代替
if (a?.b) {
}
// && 专门用于检测falsy值，如空字符串、0、NaN、null和false等。而?.只会检测null或undefined
// 可选元素访问
function tryGetArrayElement<T>(arr?: T[], index: number = 0) {
  return arr?.[index];
}
// 可选链与函数调用
// 尝试调用一个可能不存在的方法时也可以使用可选链。
let result = obj.customMethod?.();
// 可选链的运算行为被局限在属性的访问、调用以及元素的访问。
```

## ??空值合并预算符

当左侧操作数为 null 或 undefined 时，其返回右侧的操作数，否则返回左侧的操作数。

```typescript
const foo = null ?? 'default string'; // default string
const baz = 0 ?? 42; // 0
```

## 短路

当空值合并运算符的左表达式不为 null 或 undefined 时，不会对右表达式进行求值。

```typescript
function A() {
  console.log('A was called');
  return undefined;
}
function B() {
  console.log('B was called');
  return false;
}
function C() {
  console.log('C was called');
  return 'foo';
}
console.log(A() ?? C());
// A()为undefined 运行 C()
console.log(B() ?? C());
// B()为false 不运行 C()
```

空值合并运算符不能与&&和||组合使用

## ?:可选属性

TypeScript 中的接口是一个非常灵活的概念，除了可用于对类的一部分行为进行抽象以外，也常用于对 对象的形状 进行描述。

```typescript
interface Person {
  name: string;
  age?: number;
}
let semlinker: Person = {
  name: 'semlinker',
  age: 33,
};
let lolo: Person = {
  name: 'lolo',
};
```

## &运算符

## |分隔符

## \_ 数字分隔符

```typescript
const inhabitantsOfMunich = 1_464_301;
const fileSystemPermission = 0b111_111_000; // 504
```

## @XXX 装饰器

## #XXX 私有字段

