## hook简介

只运行一次的 effect（仅在组件挂载和卸载时执行），可以传递一个空数组（[]）作为第二个参数。

```javascript
useEffect(() => {
  async function fetchData() {
    const res: CommonResult = await login({});
    if (res.success) {
      setUserInfo(res.data);
    }
  }
  fetchData();
}, []);
// 无第二个参数，在挂载和 dom 每次更新时都是运行
// []); 空数组[]仅在组件挂载和卸载时执行
// [props.friend.id]); 仅在 props.friend.id 发生变化时执行
```

在挂载和 dom 每次更新时都是运行

```javascript
useEffect(() => {
  async function fetchData() {
    const res: CommonResult = await login({});
    if (res.success) {
      setUserInfo(res.data);
    }
  }
  fetchData();
});
```

在 React 的函数组件中调用 Hook
在自定义 Hook 中调用其他 Hook

只要 Hook 的调用顺序在多次渲染之间保持一致，React 就能正确地将内部 state 和对应的 Hook 进行关联。

```javascript
// 🔴 在条件语句中使用 Hook 违反第一条规则
if (name !== '') {
  useEffect(function persistForm() {
    localStorage.setItem('formData', name);
  });
}
useState('Mary'); // 1. 读取变量名为 name 的 state（参数被忽略）
// useEffect(persistForm)  // 🔴 此 Hook 被忽略！
useState('Poppins'); // 🔴 2 （之前为 3）。读取变量名为 surname 的 state 失败
useEffect(updateTitle); // 🔴 3 （之前为 4）。替换更新标题的 effect 失败
```

这就是为什么 Hook 需要在我们组件的最顶层调用。如果我们想要有条件地执行一个 effect，可以将判断放到 Hook 的内部：

```javascript
useEffect(function persistForm() {
  // 👍 将条件判断放置在 effect 中
  if (name !== '') {
    localStorage.setItem('formData', name);
  }
});
```

## hooks最佳实践

- 在使用 useEffect 时，应当抛开在 class 组件中关于生命周期的思维。
- 对于其中任一个 props ，其值在声明时便已经决定，不会随着时间产生变化。
- 状态变更时，应该通过 setState 的函数形式来代替直接获取当前状态：setCount(c => c + 1);。
- 对于函数，使用 useCallback 避免重复创建；
- 对于对象或者数组，则可以使用 useMemo。从而减少 依赖数组 (deps) 的变化。
- 对于一些变量不希望引起 effect 重新更新的，使用 ref 解决。
- 对于获取状态用于计算新的状态的，尝试 setState 的函数入参，或者使用 useReducer 整合多个类型的状态。

### useMemo

useMemo 的含义是，通过一些变量计算得到新的值。通过把这些变量加入依赖 deps，当 deps 中的值均未发生变化时，跳过这次计算。

```javascript
const data = useMemo(() => ({
    a,
    b,
    c,
    d: 'xxx'
}), [a, b, c]);
```
useMemo 中传入的函数，将在 render 函数调用过程被同步调用。可以使用 useMemo 缓存一些相对耗时的计算。
可以用 useCallback 代替

```javascript
const fn = useMemo(() => () => {
    // do something
}, [a, b]); 
```

useMemo 也非常适合用于存储引用类型的数据，可以传入对象字面量，匿名函数等，甚至是 React Elements。
```javascript
const memoComponentsA = useMemo(() => (
    <ComponentsA {...someProps} />
), [someProps]); 
```

`useMemo 的目的其实是尽量使用缓存的值。`

```javascript
function Example(props) {
    const [count, setCount] = useState(0);
    const [foo] = useState("foo");
    const main = (
        <div>
            <Item key={1} x={1} foo={foo} />
            <Item key={2} x={2} foo={foo} />
            <Item key={3} x={3} foo={foo} />
            <Item key={4} x={4} foo={foo} />
            <Item key={5} x={5} foo={foo} />
        </div>
    );
    return (
        <div>
            <p>{count}</p>
            <button onClick={() => setCount(count + 1)}>setCount</button>
            {main}
        </div>
    );
} 
// 默认情况下，每次 setCount 改变 count 的值，便会重新对 <Example> 进行 render，其返回的 React Elements 中5个 <Item> 也重新 render，
// 其耗时的操作阻塞了 UI 的渲染。导致按下 “setCount” 按钮后出现了明显的卡顿。
function Example(props) {
    const [count, setCount] = useState(0);
    const [foo] = useState("foo");

    const main = useMemo(() => (
        <div>
            <Item key={1} x={1} foo={foo} />
            <Item key={2} x={2} foo={foo} />
            <Item key={3} x={3} foo={foo} />
            <Item key={4} x={4} foo={foo} />
            <Item key={5} x={5} foo={foo} />
        </div>
    ), [foo]);

    return (
        <div>
            <p>{count}</p>
            <button onClick={() => setCount(count + 1)}>setCount</button>
            {main}
        </div>
    );
}
// 使用 useMemo，使 count 属性变化时，main 不重复 render。避免了组件拆分
```

### useEffect

- useEffect 对于函数依赖，尝试将该函数放置在 effect 内，或者使用 useCallback 包裹；
- useEffect/useCallback/useMemo，对于 state 或者其他属性的依赖，根据 eslint 的提示填入 deps；
- 如果不直接使用 state，只是想修改 state，用 setState 的函数入参方式（setState(c => c + 1)）代替；
- 如果修改 state 的过程依赖了其他属性，尝试将 state 和属性聚合，改写成 useReducer 的形式。
- 当这些方法都不奏效，使用 ref，但是依然要谨慎操作。

