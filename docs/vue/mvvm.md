# 响应式原理

## 响应式对象

### initState

在 Vue 的初始化阶段`_init`方法执行的时候，会执行`initState(vm)`方法，它的定义在`src/core/instance/state.js`中。

```javascript
export function initState(vm: Component) {
  vm._watchers = [];
  const opts = vm.$options;
  if (opts.props) initProps(vm, opts.props);
  if (opts.methods) initMethods(vm, opts.methods);
  if (opts.data) {
    initData(vm);
  } else {
    observe((vm._data = {}), true);
  }
  if (opts.computed) initComputed(vm, opts.computed);
  if (opts.watch && opts.watch !== nativeWatch) {
    initWatch(vm, opts.watch);
  }
}
```

initState 对 props、methods、data、computed、watch 等属性做了初始化操作。

- **initProps**

```javascript
function initProps(vm: Component, propsOptions: Object) {
  const propsData = vm.$options.propsData || {};
  const props = (vm._props = {});
  // ...
  // 遍历props配置
  for (const key in propsOptions) {
    // ...
    const value = validateProp(key, propsOptions, propsData, vm);
    // 把prop的值变成响应式，并能通过vm._props.xxx访问到对应的值
    defineReactive(props, key, value);
    // 把vm._props.xxx的访问代理到vm.xxx上
    proxy(vm, '_props', key);
  }
  // ...
}
```

- **initData**

```javascript
function initData(vm: Component) {
  let data = vm.$options.data;
  data = vm._data = typeof data === 'function' ? getData(data, vm) : data || {};
  // ...
  // proxy data on instance
  const keys = Object.keys(data);
  const props = vm.$options.props;
  const methods = vm.$options.methods;
  let i = keys.length;
  while (i--) {
    const key = keys[i];
    // ...
    // 把vm._data.xxx代理到vm.xxx上
    proxy(vm, `_data`, key);
  }
  // observe data
  // 把data变成响应式，通过 vm._data.xxx 访问到data中对应的属性
  observe(data, true /* asRootData */);
}
```

- **proxy**

比如：访问 this.xxx 时，通过 proxy 代理访问到 this.\_props.xxx。

```javascript
const sharedPropertyDefinition = {
  enumerable: true,
  configurable: true,
  get: noop,
  set: noop,
};
//  proxy(vm, '_props', key);
export function proxy(target: Object, sourceKey: string, key: string) {
  sharedPropertyDefinition.get = function proxyGetter() {
    return this[sourceKey][key];
  };
  sharedPropertyDefinition.set = function proxySetter(val) {
    this[sourceKey][key] = val;
  };
  // 把this[sourceKey][key]的读写变成了对target[key]的读写
  Object.defineProperty(target, key, sharedPropertyDefinition);
}
```

- **observe**

observe 的功能就是用来监测数据的变化，它的定义在 src/core/observer/index.js 中：

```javascript
// observe(data, true /* asRootData */);
export function observe(value: any, asRootData: ?boolean): Observer | void {
  if (!isObject(value) || value instanceof VNode) {
    return;
  }
  let ob: Observer | void;
  if (hasOwn(value, '__ob__') && value.__ob__ instanceof Observer) {
    ob = value.__ob__;
  } else if (
    shouldObserve &&
    !isServerRendering() &&
    (Array.isArray(value) || isPlainObject(value)) &&
    Object.isExtensible(value) &&
    !value._isVue
  ) {
    ob = new Observer(value);
  }
  if (asRootData && ob) {
    ob.vmCount++;
  }
  return ob;
}
```

observe 方法的作用就是给非 VNode 的对象类型数据添加一个 Observer，如果已经添加过则直接返回，否则在满足一定条件下去实例化一个 Observer 对象实例。

- **Observer**

```javascript
// new Observer(value);
export class Observer {
  value: any;
  dep: Dep;
  vmCount: number;
  // value是data对象
  constructor(value: any) {
    this.value = value;
    this.dep = new Dep(); // 先实例化Dep对象
    this.vmCount = 0;
    // def函数把自身实例添加到数据对象value的__ob__属性上
    def(value, '__ob__', this);
    if (Array.isArray(value)) {
      const augment = hasProto ? protoAugment : copyAugment;
      augment(value, arrayMethods, arrayKeys);
      this.observeArray(value);
    } else {
      this.walk(value);
    }
  }
  walk(obj: Object) {
    const keys = Object.keys(obj);
    for (let i = 0; i < keys.length; i++) {
      defineReactive(obj, keys[i]);
    }
  }
  observeArray(items: Array<any>) {
    for (let i = 0, l = items.length; i < l; i++) {
      observe(items[i]);
    }
  }
}
```

Observer 的构造函数逻辑很简单，首先实例化 Dep 对象，接着通过执行 def 函数把自身实例添加到数据对象 value 的 `__ob__` 属性上。
def 的定义：

```javascript
/* {
  data: Object
  pageNo: 1
  pageSize: 10
  __ob__: Observer
  dep: Dep {id: 20414, subs: Array(9)}
  value: {__ob__: Observer}
  vmCount: 0
  __proto__: Object
} */
// def(value, '__ob__', this); value就是data，this是Observer
export function def(obj: Object, key: string, val: any, enumerable?: boolean) {
  Object.defineProperty(obj, key, {
    value: val,
    enumerable: !!enumerable,
    writable: true,
    configurable: true,
  });
}
```

这里`data.__ob__ = Observer`。回到 Observer 构造函数，接下来会对 value 做判断，对于数组会调用 observeArray 方法，否则对纯对象调用 walk 方法，observeArray 是遍历数组再次调用 observe 方法，而 walk 方法是遍历对象的 key 调用 defineReactive 方法。

- **defineReactive**

defineReactive 的功能就是定义一个响应式对象，给对象动态添加 getter 和 setter，它的定义在 src/core/observer/index.js 中：

```javascript
// for (let i = 0; i < keys.length; i++) {
//    defineReactive(obj, keys[i]);
// }
export function defineReactive(
  obj:Object,
  key:string,
  val:any,
  customSetter?:?Function
  shallow?: boolean
){
  const dep = new Dep()
  const property = Object.getOwnPropertyDescriptor(obj, key)
  if (property && property.configurable === false) {
    return
  }

  // cater for pre-defined getter/setters
  const getter = property && property.get
  const setter = property && property.set
  if ((!getter || setter) && arguments.length === 2) {
    val = obj[key]
  }
  // 对子对象递归调用observe方法，这样就保证了无论 obj 的结构多复杂，它的所有子属性也能变成响应式的对象，这样我们访问或修改 obj 中一个嵌套较深的属性，也能触发 getter 和 setter。
  let childOb = !shallow && observe(val)
  Object.defineProperty(obj, key, {
    enumerable: true,
    configurable: true,
    get: function reactiveGetter () {
      const value = getter ? getter.call(obj) : val
      if (Dep.target) {
        dep.depend()
        if (childOb) {
          childOb.dep.depend()
          if (Array.isArray(value)) {
            dependArray(value)
          }
        }
      }
      return value
    },
    set: function reactiveSetter (newVal) {
      const value = getter ? getter.call(obj) : val
      /* eslint-disable no-self-compare */
      if (newVal === value || (newVal !== newVal && value !== value)) {
        return
      }
      /* eslint-enable no-self-compare */
      if (process.env.NODE_ENV !== 'production' && customSetter) {
        customSetter()
      }
      if (setter) {
        setter.call(obj, newVal)
      } else {
        val = newVal
      }
      childOb = !shallow && observe(newVal)
      // 调用dep的notify方法
      dep.notify()
    }
  })
}
```

## 依赖收集

在`defineReactive`方法中，关注两个点，一个是`const dep=new Dep()`实例化一个`Dep`实例，另一个是在`get`函数中通过`dep.depend`做依赖收集。

- **Dep**

Dep 是整个 getter 依赖收集的核心，它的定义在 src/core/observer/dep.js 中：

```javascript
export default class Dep {
  // 全局唯一Watcher 同一时间只有一个watcher被计算
  static target: ?Watcher;
  id: number;
  // Watcher数组
  subs: Array<Watcher>;
  constructor() {
    this.id = uid++;
    this.subs = [];
  }
  addSub(sub: Watcher) {
    this.subs.push(sub);
  }
  removeSub(sub: Watcher) {
    remove(this.subs, sub);
  }
  depend() {
    if (Dep.target) {
      Dep.target.addDep(this);
    }
  }
  notify() {
    const subs = this.subs.slice();
    for (let i = 0; i < subs.length; i++) {
      subs[i].update();
    }
  }
}
Dep.target = null;
const targetStack = [];
export function pushTarget(_target: ?Watcher) {
  if (Dep.target) targetStack.push(Dep.target);
  Dep.target = _target;
}
export function popTarget() {
  Dep.target = targetStack.pop();
}
```

Dep 有一个静态属性`target`，这是一个全局唯一`Watcher`。在同一时间只能有一个全局的`Watcher`被计算，另外它的自身属性`subs`也是 `Watcher`的数组。
Dep 实际上就是对 Watcher 的一种管理，Dep 脱离 Watcher 单独存在是没有意义的，为了完整地讲清楚依赖收集过程，我们有必要看一下 Watcher 的一些相关实现，它的定义在 src/core/observer/watcher.js 中：

```javascript
let uid = 0;
export default class Watcher {
  vm: Component;
  expression: string;
  cb: Function;
  id: number;
  deep: boolean;
  user: boolean;
  computed: boolean;
  sync: boolean;
  dirty: boolean;
  active: boolean;
  dep: Dep;
  deps: Array<Dep>;
  newDeps: Array<Dep>;
  depIds: SimpleSet;
  newDepIds: SimpleSet;
  before: ?Function;
  getter: Function;
  value: any;

  constructor(
    vm: Component,
    expOrFn: string | Function,
    cb: Function,
    options?: ?Object,
    isRenderWatcher?: boolean
  ) {
    this.vm = vm;
    if (isRenderWatcher) {
      vm._watcher = this;
    }
    vm._watchers.push(this);
    // options
    if (options) {
      this.deep = !!options.deep;
      this.user = !!options.user;
      this.computed = !!options.computed;
      this.sync = !!options.sync;
      this.before = options.before;
    } else {
      this.deep = this.user = this.computed = this.sync = false;
    }
    this.cb = cb;
    this.id = ++uid; // uid for batching
    this.active = true;
    this.dirty = this.computed; // for computed watchers
    this.deps = [];
    this.newDeps = [];
    this.depIds = new Set();
    this.newDepIds = new Set();
    this.expression =
      process.env.NODE_ENV !== 'production' ? expOrFn.toString() : '';
    // parse expression for getter
    if (typeof expOrFn === 'function') {
      this.getter = expOrFn;
    } else {
      this.getter = parsePath(expOrFn);
      if (!this.getter) {
        this.getter = function () {};
        process.env.NODE_ENV !== 'production' &&
          warn(
            `Failed watching path: "${expOrFn}" ` +
              'Watcher only accepts simple dot-delimited paths. ' +
              'For full control, use a function instead.',
            vm
          );
      }
    }
    if (this.computed) {
      this.value = undefined;
      this.dep = new Dep();
    } else {
      // 在Watcher构造函数里执行this.get()方法
      this.value = this.get();
    }
  }
  get() {
    /**
     * 执行pushTarget方法
     *
     * 实际上就是把Dep.target赋值为当前的渲染watcher并压栈
     */
    pushTarget(this);
    let value;
    const vm = this.vm;
    try {
      /**
       * 再执行this.getter方法
       * new Watcher(vm, updateComponent, noop, ...
       * this.getter = updateComponent
       * 就是在执行vm._update(vm._render(),hydrating)
       * vm._render()方法会生成渲染VNode，这个过程会对vm上的数据访问，
       * 就会触发数据属性的getter
       */
      //
      value = this.getter.call(vm, vm);
    } catch (e) {
      if (this.user) {
        handleError(e, vm, `getter for watcher "${this.expression}"`);
      } else {
        throw e;
      }
    } finally {
      // "touch" every property so they are all tracked as
      // dependencies for deep watching
      if (this.deep) {
        traverse(value);
      }
      popTarget();
      this.cleanupDeps();
    }
    return value;
  }

  /**
   * Add a dependency to this directive.
   */
  addDep(dep: Dep) {
    const id = dep.id;
    if (!this.newDepIds.has(id)) {
      this.newDepIds.add(id);
      this.newDeps.push(dep);
      if (!this.depIds.has(id)) {
        dep.addSub(this);
      }
    }
  }

  /**
   * Clean up for dependency collection.
   */
  cleanupDeps() {
    let i = this.deps.length;
    while (i--) {
      const dep = this.deps[i];
      if (!this.newDepIds.has(dep.id)) {
        dep.removeSub(this);
      }
    }
    let tmp = this.depIds;
    this.depIds = this.newDepIds;
    this.newDepIds = tmp;
    this.newDepIds.clear();
    tmp = this.deps;
    this.deps = this.newDeps;
    this.newDeps = tmp;
    this.newDeps.length = 0;
  }
  // ...
}
```

Watcher 是一个 Class，在它的构造函数中，定义了一些和 Dep 相关的属性：

```javascript
this.deps = [];
this.newDeps = [];
this.depIds = new Set();
this.newDepIds = new Set();
```

`this.deps` 和 `this.newDeps` 表示 Watcher 实例持有的 Dep 实例的数组；而 `this.depIds` 和 `this.newDepIds` 分别代表 `this.deps` 和 `this.newDeps` 的 id Set。

## 过程分析

对数据对象的访问会触发他们的 `getter` 方法，Vue 的`mount`过程是通过`mountComponent`函数。

```javascript
updateComponent = () => {
  vm._update(vm._render(), hydrating);
};
new Watcher(
  vm,
  updateComponent,
  noop,
  {
    before() {
      if (vm._isMounted) {
        callHook(vm, 'beforeUpdate');
      }
    },
  },
  true /* isRenderWatcher */
);
```

实例化 Watcher 的时候，在 Watcher 的构造函数里，会执行 this.get()方法，get 函数里会执行 pushTarget(this)。

```javascript
export function pushTargrt(_targrt: Wacher) {
  if (Dep.target) targetStack.push(Dep.target);
  Dep.target = _target;
}
```

把 Dep.target 赋值为当前的渲染 watcher 并压栈。接着又执行了：

```javascript
value = this.getter.call(vm, vm);
```

this.getter 对应就是 updateComponent 函数，这实际上就是在执行：

```javascript
vm._update(vm._render(), hydrating);
```

它会先执行 vm.\_render() 方法，因为之前分析过这个方法会生成 渲染 VNode，并且在这个过程中会对 vm 上的数据访问，这个时候就触发了数据对象的 getter。那么每个对象值的 getter 都持有一个 dep，在触发 getter 的时候会调用 dep.depend() 方法，也就会执行 Dep.target.addDep(this)。刚才我们提到这个时候 Dep.target 已经被赋值为渲染 watcher，那么就执行到 addDep 方法：

```javascript
addDep (dep: Dep) {
  const id = dep.id
  if (!this.newDepIds.has(id)) {
    this.newDepIds.add(id)
    this.newDeps.push(dep)
    if (!this.depIds.has(id)) {
      dep.addSub(this)
    }
  }
}
```

这时候会做一些逻辑判断（保证同一数据不会被添加多次）后执行 dep.addSub(this)，那么就会执行 this.subs.push(sub)，也就是说把当前的 watcher 订阅到这个数据持有的 dep 的 subs 中，这个目的是为后续数据变化时候能通知到哪些 subs 做准备。
所以在 vm.\_render() 过程中，会触发所有数据的 getter，这样实际上已经完成了一个依赖收集的过程。

```javascript
if (this.deep) {
  traverse(value);
}
```

这个是要递归去访问 value，触发它所有子项的 getter，这个之后会详细讲。接下来执行：

```javascript
popTarget();
```

popTarget 的定义在 src/core/observer/dep.js 中：

```javascript
Dep.target = targetStack.pop();
```

实际上就是把 Dep.target 恢复成上一个状态，因为当前 vm 的数据依赖收集已经完成，那么对应的渲染 Dep.target 也需要改变。最后执行：

```javascript
this.cleanupDeps();
```

依赖清空的过程，

```javascript
cleanupDeps () {
  let i = this.deps.length
  while (i--) {
    const dep = this.deps[i]
    if (!this.newDepIds.has(dep.id)) {
      dep.removeSub(this)
    }
  }
  let tmp = this.depIds
  this.depIds = this.newDepIds
  this.newDepIds = tmp
  this.newDepIds.clear()
  tmp = this.deps
  this.deps = this.newDeps
  this.newDeps = tmp
  this.newDeps.length = 0
}
```

考虑到 Vue 是数据驱动的，所以每次数据变化都会重新 render，那么 vm.\_render() 方法又会再次执行，并再次触发数据的 getters，所以 Wathcer 在构造函数中会初始化 2 个 Dep 实例数组，newDeps 表示新添加的 Dep 实例数组，而 deps 表示上一次添加的 Dep 实例数组。
在执行 cleanupDeps 函数的时候，会首先遍历 deps，移除对 dep.subs 数组中 Wathcer 的订阅，然后把 newDepIds 和 depIds 交换，newDeps 和 deps 交换，并把 newDepIds 和 newDeps 清空。

## 派发更新

setter 的逻辑有 2 个关键的点，一个是 childOb = !shallow && observe(newVal)，如果 shallow 为 false 的情况，会对新设置的值变成一个响应式对象；另一个是 dep.notify()，通知所有的订阅者，

```javascript
set: function reactiveSetter(newVal) {
  const value = getter ? getter.call(obj) : val;
  /* eslint-disable no-self-compare */
  if (newVal === value || (newVal !== newVal && value !== value)) {
    return;
  }
  /* eslint-enable no-self-compare */
  if (process.env.NODE_ENV !== 'production' && customSetter) {
    customSetter();
  }
  if (setter) {
    setter.call(obj, newVal);
  } else {
    val = newVal;
  }
  childOb = !shallow && observe(newVal);
  dep.notify();
}
```
dep.notify() 方法， 它是 Dep 的一个实例方法，定义在 src/core/observer/dep.js 中：
```javascript
class Dep {
  // ...
  notify () {
  // stabilize the subscriber list first
    const subs = this.subs.slice()
    for (let i = 0, l = subs.length; i < l; i++) {
      subs[i].update()
    }
  }
}
```
这里的逻辑非常简单，遍历所有的 subs，也就是 Watcher 的实例数组，然后调用每一个 watcher 的 update 方法，它的定义在 src/core/observer/watcher.js 中：
```javascript
class Watcher {
  // ...
  update () {
    /* istanbul ignore else */
    if (this.computed) {
      // A computed property watcher has two modes: lazy and activated.
      // It initializes as lazy by default, and only becomes activated when
      // it is depended on by at least one subscriber, which is typically
      // another computed property or a component's render function.
      if (this.dep.subs.length === 0) {
        // In lazy mode, we don't want to perform computations until necessary,
        // so we simply mark the watcher as dirty. The actual computation is
        // performed just-in-time in this.evaluate() when the computed property
        // is accessed.
        this.dirty = true
      } else {
        // In activated mode, we want to proactively perform the computation
        // but only notify our subscribers when the value has indeed changed.
        this.getAndInvoke(() => {
          this.dep.notify()
        })
      }
    } else if (this.sync) {
      this.run()
    } else {
      queueWatcher(this)
    }
  }
}  
```
这里对于 Watcher 的不同状态，会执行不同的逻辑，computed 和 sync 等状态的分析我会之后抽一小节详细介绍，在一般组件数据更新的场景，会走到最后一个 queueWatcher(this) 的逻辑，queueWatcher 的定义在 src/core/observer/scheduler.js 中：
```javascript
const queue: Array<Watcher> = []
let has: { [key: number]: ?true } = {}
let waiting = false
let flushing = false
/**
 * Push a watcher into the watcher queue.
 * Jobs with duplicate IDs will be skipped unless it's
 * pushed when the queue is being flushed.
 */
export function queueWatcher (watcher: Watcher) {
  const id = watcher.id
  if (has[id] == null) {
    has[id] = true
    if (!flushing) {
      queue.push(watcher)
    } else {
      // if already flushing, splice the watcher based on its id
      // if already past its id, it will be run next immediately.
      let i = queue.length - 1
      while (i > index && queue[i].id > watcher.id) {
        i--
      }
      queue.splice(i + 1, 0, watcher)
    }
    // queue the flush
    if (!waiting) {
      waiting = true
      nextTick(flushSchedulerQueue)
    }
  }
}
```