var express = require('express');
var app = express();


app.post('/login', function (req, res) {
  console.log(req.body);
  res.end('hello');
});

app.listen(3000);

function Events () {
  this.clientList = {}
};
Events.prototype.on = function (key, fn, ...args) {
  if (!this.clientList[key]) {
    this.clientList[key] = []
  }
  this.clientList[key].push({
    fn,
    args,
    once:false,
  })
}
Events.prototype.fire=function(key,...args){
  this.clientList[key].map((event,i)=>{
    event.fn(...event.args,...args)
    if(event.once){
      this.clientList[key].splice(i,1)
    }
  })
}
Events.prototype.off=function(key,fn){
  this.clientList[key].map((event,i)=>{
    if(event.fn===fn){
      this.clientList[key].splice(i,1)
    }
  })
  
}
Events.prototype.once=function(key,fn,...args){
  if (!this.clientList[key]) {
    this.clientList[key] = []
  }
  this.clientList[key].push({
    fn,
    args,
    once:true,
  })
}
const fn1 = (... args)=>console.log('I want sleep1', ... args)
const fn2 = (... args)=>console.log('I want sleep2', ... args)
const event = new Events();
event.on('sleep', fn1, 1, 2, 3);
event.on('sleep', fn2, 1, 2, 3);
event.fire('sleep', 4, 5, 6);
// I want sleep1 1 2 3 4 5 6
// I want sleep2 1 2 3 4 5 6
event.off('sleep', fn1);
event.once('sleep', () => console.log('I want sleep'));
event.fire('sleep');
// I want sleep2 1 2 3
// I want sleep
event.fire('sleep');
// I want sleep2 1 2 3